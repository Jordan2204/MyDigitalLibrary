<?php

namespace App\Http\Livewire\DataTable;

use App\Models\Subject;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class SubjectsTable extends LivewireDatatable
{
    public $model = Subject::class;

    public $hideable = 'select';
    public $exportable = true;
    
    protected $listeners =
     [
        'newQuery' => 'refresh_data',
        'updateQuery' => 'refresh_data',
        'deleteQuery' => 'refresh_data'
    ];

    public function builder()
    {
        return  Subject::query();
    }

    public function showEditModal($id){
        $this->emitUp('showEditModal', $id);
    }

    public function showDeleteModal($id){
        $this->emitUp('showDeleteModal', $id);
    }

    
    public function refresh_data()
    {
        $this->emit('updateSavedQueries');
    }

  
    public function columns()
    {
        return [
           
            Column::callback(['id', 'cover'], function ($id, $cover) {
                return view('datatables.table-cover', 
                ['id' => $id, 'cover' => $cover]);
            })->label(__('Cover'))
            ->unsortable(),

            Column::name('title')
                ->label(__('Title'))
                ->searchable()
                ->alignCenter(),

            Column::name('author.name')
                ->label(__('Author'))
                ->searchable(),
            
            Column::name('field.abbr')
            ->label(__('Field'))
            ->searchable(),

            Column::name('level.abbr')
            ->label(__('Level'))
            ->searchable(),

            Column::name('period.title')
            ->label(__('Period'))
            ->searchable(),


            Column::callback(['id', 'title'], function ($id, $title) {
                return view('datatables.table-actions', 
                ['id' => $id, 'name' => $title, 'type' => "subject",]);
            })->label('Actions')
            ->unsortable()
           

        ];
    }
}