<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    protected $table = 'subjects';
    protected $guarded = ['created_at'];
   
    //Make it available in the json response
    protected $appends = [
        'average_rating',
        'isLiked',
        'model_name'
    ];

    //implement the attribute
    public function getModelNameAttribute()
    {
        return 'Subject';
    }

    public function author(){
        return $this->belongsTo(Author::class);
    }

    public function views(){
    return $this->morphMany(View::class,'view');
    }

    public function level(){
        return $this->belongsTo(Level::class, "level_id");
    }

    public function field(){
        return $this->belongsTo(Field::class);
    }

    public function period(){
        return $this->belongsTo(Period::class, 'period_id');
    }

    public function school(){
        return $this->belongsTo(School::class, 'school_id');
    }

    public function likes(){
        return $this->morphMany(Like::class, "like");
    }

    public function isLiked()
    {
        if (!auth()->check()) {
            return false;
        }
     
       return auth()->user()->likes->contains(function ($value, $key) {
            return $value->like_type == Subject::class &&  $value->like_id == $this->id;
        });
    }

    public function marks(){
        return $this->morphMany(Mark::class, "mark");
    }
    
    function getAverageRatingAttribute(){
        return round($this->marks()->avg('value'),1);
    }

    function getIsLikedAttribute(){
        return $this->isLiked();
    }

}
