<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('testimonials')->insert([
            'title' => "Software Engineer",
            'username' => "Jordan",
            'description' => "Cette platforme me fais gagner tu temps car c'est facile de consulter des ouvrages.",
            'profile_photo_path' =>"/dist/images/user_profile.png",
            'created_at' => Now(),
            'updated_at' => Now(),
        ]);

        DB::table('testimonials')->insert([
            'title' => "Student",
            'username' => "Carine",
            'description' => "Cette platforme me fais gagner tu temps car c'est facile de consulter des ouvrages.",
            'profile_photo_path' =>"/dist/images/user_profile.png",
            'created_at' => Now(),
            'updated_at' => Now(),
        ]);


        DB::table('testimonials')->insert([
            'title' => "Librarian",
            'username' => "Vincent",
            'description' => "Cette platforme me fais gagner tu temps car c'est facile de consulter des ouvrages.",
            'profile_photo_path' =>"/dist/images/user_profile.png",
            'created_at' => Now(),
            'updated_at' => Now(),
        ]);



        DB::table('testimonials')->insert([
            'title' => "Student",
            'username' => "PAul Kamdem",
            'description' => "Cette platforme me fais gagner tu temps car c'est facile de consulter des ouvrages.",
            'profile_photo_path' =>"/dist/images/user_profile.png",
            'created_at' => Now(),
            'updated_at' => Now(),
        ]);
    }
}
