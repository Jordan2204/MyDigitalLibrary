<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use App\Actions\Fortify\PasswordValidationRules;
use Laravel\Fortify\Contracts\ResetsUserPasswords;
use phpDocumentor\Reflection\Types\This;

class Members extends Component
{
    use WithPagination;
    use PasswordValidationRules;
    use WithFileUploads;
 

    public $member_id;
    public $matricule;
    public $name;
    public $email;
    public $phone;
    public $country;
    public $city;
    public $date_of_birth;
    public $role;
    public $image;  
    public $newImage;
    public $password;
    public $password_confirmation;

    public $showModalForm = false;
    public $showDeleteModalForm = false;

    public function showCreateMemberModal()
    {
        $this->resetValidation();
        $this->showModalForm = true;
    }
    public function updatedShowModalForm()
    {
        $this->reset();
    }
    
    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }

    public function storeMember()
    {
        $this->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'image' => 'image|max:1024|nullable',
            'password' => $this->passwordRules(),
        ]);

        $image_name ="";
       if ($this->image) {
            $url_extension = $this->image->getClientOriginalExtension();
            $image_name = 'profile-photos'.'/'.Str::random(40);
            $image_name = $image_name.'.'.$url_extension;
            $this->image->storeAs('public/', $image_name );
        }
        $member = User::create([
            'matricule' => $this->matricule,
            'name' => $this->name,
            'date_of_birth' => $this->date_of_birth,
            'profile_photo_path' => $image_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'password' => Hash::make($this->password),
        ]);

        $member->assignRole($this->role);

        if($this->role != "Member"){
            $member->assignRole("Member");
        }

        $this->reset();
       
       $this->alert('success', 'Member created Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }
    public function updateMember()
    {
        $this->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'image' => 'image|max:1024|nullable',
        ]);

      if ($this->image) {
        Storage::delete('public/profile-photos/', $this->newImage);
        $this->newImage = $this->image->getClientOriginalName();
        $this->image->storeAs('public/profile-photos/',Str::random(40));
    }

        User::find($this->member_id)->update([
            'matricule' => $this->matricule,
            'name' => $this->name,
            'date_of_birth' => $this->date_of_birth,
            'email' => $this->email,
            'phone' => $this->phone,
            'password' => Hash::make($this->password),
            'profile_photo_url' => $this->newImage
      
        ]);
        $this->reset();
        $this->alert('success', 'Member updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

    public function showEditMemberModal($id)
    {
        $this->reset();
        $this->showModalForm = true;
        $this->member_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $member = User::findOrFail($this->member_id);
        $this->matricule =  $member->matricule;
        $this->name =  $member->name;
        $this->date_of_birth =  $member->date_of_birth;
        //$this->city =  $member->city;
        //$this->country =  $member->country;
        $this->email =  $member->email;
        $this->role = $member->getRoleNames();
        $this->newImage = $member->profile_photo_url;
    }

    public function showDeleteMemberModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->member_id = $id;
    }

    public function deleteMember($id)
    {
        $member = User::find($id);
        if($member->profile_photo_path){
            Storage::delete('public/', $member->profile_photo_path);
        }

        //Delete the views of a user
        $member->views()->delete();
      
        $this->reset();
        $this->showDeleteModalForm = false;
        $this->alert('success', 'Member deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }
    public function render()
    {
        return view('livewire.members',
        [
        'members' => User::role('Member')->with('roles')->get(),
        'roles' => Role::where("name", "Student")->orWhere("name", "Lecturer")->orWhere("name", "Staff")->get()
        ]);
    }
}
