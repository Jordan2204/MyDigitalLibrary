<div>
    <div class="relative z-0 intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ __('Subjects Management') }}</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <x-jet-button wire:click="showCreateModal" class=" bg-theme-1 shadow-md mr-2 text-center">New Subject</x-jet-button>
            <div class="dropdown relative ml-auto sm:ml-0">
                <button class="dropdown-toggle button px-2 box text-gray-700">
                    <span class="w-5 h-5 flex items-center justify-center">
                        <i class="w-4 h-4" data-feather="plus"></i>
                    </span>
                </button>
                <div class="dropdown-box mt-10 absolute w-40 top-0 right-0">
                    <div class="dropdown-box__content box p-2">
                        <a href="" class="flex items-center p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                           {{ __(' New Field') }}
                        </a>
                        <a href="" class="flex items-center p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                           {{ __(' New School') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: Datatable -->
    <div class="intro-y datatable-wrapper box p-2 pt-2 mt-5">
        @livewire('data-table.subjects-table')
    </div>
    <!-- END: Datatable -->
        <!-- Modal create Subject-->
<x-jet-dialog-modal :maxWidth="'6xl'" class="z-40" wire:model="showModalForm">
    @if($subject_id)
        <x-slot name="ico">
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-2 ml-2 h-6 w-6 text-yellow-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
            </svg>
        </x-slot>
        <x-slot name="title">{{__("Updating a Subject") }}</x-slot>   
    @else
        <x-slot name="ico">
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-2 ml-2 h-6 w-6 text-green-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
        </x-slot>
        <x-slot name="title">{{ __("Creation of a Subject") }}</x-slot>  
    @endif
    <x-slot name="close">
        <a x-on:click.prevent @click="@this.closeModal()" href=""> 
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-1 ml-2 text-white h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </a> 
    </x-slot>
 <x-slot name="content">
   <div class="space-y-4 divide-y divide-gray-200">
    @if($subject_id)
    <form id="form_create" wire:submit.prevent="update" autocomplete="off" enctype="multipart/form-data">
    @else
    <form id="form_create" wire:submit.prevent="store" autocomplete="off" enctype="multipart/form-data">
    @endif   
     @csrf
            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 text sm:pb-4"> 
                <div class="grid grid-cols-1 sm:grid-cols-3 gap-4 mb-3">
                    <div class="grid grid-cols-1 sm:grid-cols-2 col-span-2 gap-4">
                       
                        <div class="col-span-2 sm:col-span-1"> 
                            <x-jet-label for="title" value="{{ __('Title') }}" />
                            <input id="title" name="title" wire:model.lazy="title" type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2" placeholder="title" autofocus autocomplete="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div> 
            
                        <div class="col-span-2 sm:col-span-1"> 
                            <x-jet-label for="academic_year" value="{{ __('Year') }}" />
                            <input id="academic_year" name="academic_year" wire:model.lazy="academic_year" type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2" placeholder="Ex : 2021" autofocus autocomplete="academic_year">
                      @error('academic_year') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div>
                        
                        <div class="col-span-2 sm:col-span-1"> 
                            <x-jet-label for="level" value="{{ __('Level') }}" />
                            <select id="level" wire:model="level" name="level" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                @if($this->level == null)
                                    <option selected value="Null">{{ __('Choose a level') }}</option>
                                @endif
                                @foreach ($levels as $level)
                                    <option value="{{ $level->id }}">{{ $level->title }}</option>
                                @endforeach
                            </select>      
                             @error('level') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div>
                        <div class="col-span-2 sm:col-span-1"> 
                            <x-jet-label for="school" value="{{ __('School') }}" />
                            <select id="school" wire:model="school" name="school" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                @if($this->school == null)
                                    <option selected value="Null">{{ __('Choose a school') }}</option>
                                @endif
                                @foreach ($schools as $school)
                                    <option value="{{ $school->id }}">{{ $school->abbr }}</option>
                                @endforeach
                            </select>      
                             @error('school') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div>

                      

                        <div class="col-span-2 sm:col-span-1"> 
                            <x-jet-label for="period" value="{{ __('Period') }}" />
                            <select id="period" wire:model="period" name="period" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                @if($this->period == null)
                                    <option selected value="Null">{{ __('Choose a period') }}</option>
                                @endif
                                @foreach ($periods as $period)
                                    <option value="{{ $period->id }}">{{ $period->title }}</option>
                                @endforeach
                            </select>      
                             @error('period') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div>

                        <div class="col-span-2 sm:col-span-1"
                            x-data="{ isUploading: false, progress: 0 }"
                            x-on:livewire-upload-start="isUploading = true"
                            x-on:livewire-upload-finish="isUploading = false"
                            x-on:livewire-upload-error="isUploading = false"
                            x-on:livewire-upload-progress="progress = $event.detail.progress"
                            > 
                            <x-jet-label for="url" value="{{ __('File') }}" />
                            <input type="file" id="url" wire:model="url" name="url" class="block w-full transition duration-150 ease-in-out appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal sm:text-sm sm:leading-5" />
                            <!-- Progress Bar -->
                            <div x-show="isUploading">
                                <progress max="100" x-bind:value="progress"></progress>
                            </div>  
                            @error('url') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div>

                        <div class="col-span-2 sm:col-span-1"> 
                            <x-jet-label for="author" value="{{ __('Author') }}" />
                            <select id="author" wire:model="author" name="author" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                @if($this->author == null)
                                    <option selected value="Null">{{ __('Choose an Author') }}</option>
                                @endif
                                @foreach ($authors as $author)
                                    @if ($this->author && $this->author == $author->id)
                                        <option selected value="{{ $author->id }}">{{ $author->name }}</option>
                                        
                                    @elseif($author->name == 'IUC')
                                        <option selected value="{{ $author->id }}">{{ $author->name }}</option>
                                        
                                    @endif
                                @endforeach
                            </select>      
                             @error('author') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div>

                        <div class="col-span-2 sm:col-span-1"> 
                            <x-jet-label for="level" class="mb-2" value="{{ __('Field') }}" />
                            <div wire:ignore>
                                <select id="field" style="width: 100%!important;" wire:model="field" name="field" data-hide-search="false" class="select2 shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                    @if($this->field == null)
                                        <option selected value="">{{ __('Choose a field') }}</option>
                                    @endif
                                    @foreach ($fields as $field)
                                        <option value="{{ $field->id }}">{{ $field->title }}</option>
                                    @endforeach
                                </select>     
                            </div>
                          
                             @error('field') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div>

                       
                    </div> 

                    <div class="grid grid-cols-1 sm:grid-cols-2 gap-4">
                        <div class="col-span-2 border border-gray-200 rounded-md p-5">
                            <div class="w-full h-60 relative image-fit cursor-pointer zoom-in mx-auto">
                                @if ($cover && !is_string($cover))
                                    <img class="rounded-md" alt="subject cover" src="{{ $cover->temporaryUrl() }}">
                                @elseif ($cover && is_string($cover))
                                    <img class="rounded-md" alt="subject cover" src="/{{ $cover }}">
                                @else
                                    <img class="rounded-md" alt="Cover Default" src="{{ asset('dist/images/awaiting_cover.jpg') }}">
                                @endif
                                <div class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 tooltipstered"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> </div>
                            </div>
                            <div class="w-40 mx-auto cursor-pointer relative mt-5">
                                <button type="button" class="button w-full bg-theme-1 text-white">Change cover</button>
                                <input type="file" id="cover" wire:model="cover" name="cover" class="w-full h-full top-0 left-0 absolute opacity-0">
                                @error('cover') <span class="error text-red-600">{{ $message }}</span> @enderror
    
                            </div>
                        </div>
                    </div>    
                </div>
            </div> 
      
    </div>
 </x-slot>

 <x-slot name="footer">
   @if($subject_id)
   <x-jet-button wire:loading.attr="disabled" type="submit">  {{ __('Update') }}</x-jet-button>
   @else
   <x-jet-button wire:loading.attr="disabled" type="submit"> {{ __('Create') }}</x-jet-button>
   @endif
    <div wire:loading class="mt-1">
        <svg width="" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" fill="rgb(45, 55, 72)" class="content-center w-6 h-6">
            <circle cx="15" cy="15" r="15">
                <animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate>
                <animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate>
            </circle>
            <circle cx="60" cy="15" r="9" fill-opacity="0.3">
                <animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite"></animate>
                <animate attributeName="fill-opacity" from="0.5" to="0.5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite"></animate>
            </circle>
            <circle cx="105" cy="15" r="15">
                <animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate>
                <animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate>
            </circle>
        </svg>
    </div>
 </x-slot>
</form> 
</x-jet-dialog-modal>

<x-jet-confirmation-modal wire:model="showDeleteModalForm">
    <x-slot name="title">{{ __('Deletion of a Subject') }}</x-slot>
    <x-slot name="close">
        <a x-on:click.prevent @click="@this.closeModal()" href=""> 
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-1 ml-2 text-white h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </a> 
    </x-slot>
    <x-slot name="content">
        <div class="space-y-8 text-2xl divide-y divide-gray-200">
            <div class="p-5 text-center"> <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                @if($this->subject_id)
                <p>{{__("Do you really want to delete this")}} <span class="text-gray-900 text-bold">{{__("Data") }} ?</span> {{ __("This process cannot be undone")}}.</p>
                @endif
            </div>      
        </div>
    </x-slot>
    <x-slot name="footer">
       <x-jet-button wire:loading.attr="disabled" wire:click="delete({{ $this->subject_id }})" class="bg-red-700"> 
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
            </svg> 
            {{__('Delete') }}
        </x-jet-button>
    </x-slot>
</x-jet-confirmation-modal>
</div>
@push('js')

  <script>
        $('#form_create').submit(function() {
            @this.set('field', $('#field').val());
            console.log($('#field').val());
         });
    </script>
@endpush