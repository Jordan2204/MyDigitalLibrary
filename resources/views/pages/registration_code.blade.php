@extends('../layouts/' . $layout)

@section('subhead')
    <title>Registration Code Management</title>
@endsection

@section('subcontent')
    @livewire('registration-code')
@endsection