<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Show specified view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loadPage($pageName = 'dashboard')
    {
        $layout = 'side-menu';
        $activeMenu = $this->activeMenu($layout, $pageName);

        return view('pages/' . $pageName, [
            'side_menu' => $this->chooseSideMenu(),
            'first_page_name' => $activeMenu['first_page_name'],
            'second_page_name' => $activeMenu['second_page_name'],
            'third_page_name' => $activeMenu['third_page_name'],
            'layout' => 'side-menu'
        ]);
    
    }

    /**
     * Determine active menu & submenu.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function activeMenu($layout, $pageName)
    {
        $firstPageName = '';
        $secondPageName = '';
        $thirdPageName = '';
      
        foreach ($this->chooseSideMenu() as $menu) {
            if ($menu !== 'devider' && $menu['page_name'] == $pageName && empty($firstPageName)) {
                $firstPageName = $menu['page_name'];
            }

            if (isset($menu['sub_menu'])) {
                foreach ($menu['sub_menu'] as $subMenu) {
                    if ($subMenu['page_name'] == $pageName && empty($secondPageName) && $subMenu['page_name'] != 'dashboard') {
                        $firstPageName = $menu['page_name'];
                        $secondPageName = $subMenu['page_name'];
                    }

                    if (isset($subMenu['sub_menu'])) {
                        foreach ($subMenu['sub_menu'] as $lastSubmenu) {
                            if ($lastSubmenu['page_name'] == $pageName) {
                                $firstPageName = $menu['page_name'];
                                $secondPageName = $subMenu['page_name'];
                                $thirdPageName = $lastSubmenu['page_name'];
                            }       
                        }
                    }
                }
            }
        }
     
        return [
            'first_page_name' => $firstPageName,
            'second_page_name' => $secondPageName,
            'third_page_name' => $thirdPageName
        ];
    }

    public function chooseSideMenu(){
        $isSuperAdmin = session('isSuperAdmin');
        $isAdmin = session('isAdmin');
        $isLibrarian = session('isLibrarian');
   
        if($isSuperAdmin){
            return $this->superAdmin();

        }elseif($isAdmin){
            return $this->admin();
            
        }elseif($isLibrarian){
            return $this->librarian();
        }
    }

    /**
     * List of side menu items.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  
    public function admin()
    {
        return [
            'dashboard' => [
                'icon' => 'home',
                'layout' => 'side-menu',
                'page_name' => 'dashboard',
                'title' => 'Dashboard'
            ],
            'works' => [
                'icon' => 'book',
                'page_name' => 'works',
                'title' => 'Works',
                'sub_menu' => [
                    'books' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'books',
                        'title' => 'Books'
                    ],
                    'student-reports' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'student-reports',
                        'title' => 'Report/Thesis'
                    ],
                    'subjects' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'subjects',
                        'title' => 'Subjects'
                    ],
                ],
            ],
            'booking' => [
                'icon' => 'clock',
                'layout' => 'side-menu',
                'page_name' => 'reservations',
                'title' => 'Bookings'
            ],
            'book-series' => [
                'icon' => 'book',
                'layout' => 'side-menu',
                'page_name' => 'book-series',
                'title' => 'Books Series'
            ],
           'devider',
           'Dewey Classification' => [
            'icon' => 'slack',
            'layout' => 'side-menu',
            'page_name' => 'dewey-classification',
            'title' => 'Dewey Classification'
        ],
           'devider',
            'users' => [
                'icon' => 'users',
                'page_name' => 'users',
                'title' => 'Users',
                'sub_menu' => [
                    'registration_code' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'registration_code',
                        'title' => 'Registration Code'
                    ],
                    'members' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'members',
                        'title' => 'Members'
                    ],
                    'admin' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'admins',
                        'title' => 'Admin'
                    ],
                    'roles' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'roles',
                        'title' => 'Roles'
                    ],
                    'permissions' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'permissions',
                        'title' => 'Permissions'
                    ],
                ],
            ],
            'tools' => [
                'icon' => 'codesandbox',
                'page_name' => 'tools',
                'title' => 'Tools',
                'sub_menu' => [
                    'schools' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'schools',
                        'title' => 'Schools'
                    ],
                    'fields' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'fields',
                        'title' => 'Fields'
                    ],
                    'authors' => [
                        'icon' => 'user',
                        'layout' => 'side-menu',
                        'page_name' => 'authors',
                        'title' => 'Authors'
                    ],
                    'publishers' => [
                        'icon' => 'user',
                        'layout' => 'side-menu',
                        'page_name' => 'publishers',
                        'title' => 'Publishers'
                    ],
                    'testimonials' => [
                        'icon' => 'user',
                        'layout' => 'side-menu',
                        'page_name' => 'testimonials',
                        'title' => 'Testimonials'
                    ]
                ]
            ],
           
            'devider',  
            'config' => [
                'icon' => 'settings',
                'page_name' => 'config',
                'title' => 'Config',
                'sub_menu' => [
                    'setting' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'setting',
                        'title' => 'Settings'
                    ],
                    'home-setting' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'home-setting',
                        'title' => 'Home Setting'
                    ]
                ]
            ]
        ];
    }

    public function librarian()
    {
        return [
            'dashboard' => [
                'icon' => 'home',
                'layout' => 'side-menu',
                'page_name' => 'dashboard',
                'title' => 'Dashboard'
            ],
            'works' => [
                'icon' => 'book',
                'page_name' => 'works',
                'title' => 'Works',
                'sub_menu' => [
                    'books' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'books',
                        'title' => 'Books'
                    ],
                    'student-reports' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'student-reports',
                        'title' => 'Report/Thesis'
                    ],
                    'subjects' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'subjects',
                        'title' => 'Subjects'
                    ],
                ],
            ],
            'booking' => [
                'icon' => 'clock',
                'layout' => 'side-menu',
                'page_name' => 'reservations',
                'title' => 'Bookings'
            ],
            'book-series' => [
                'icon' => 'book',
                'layout' => 'side-menu',
                'page_name' => 'book-series',
                'title' => 'Books Series'
            ],
           'devider',
           'Dewey Classification' => [
            'icon' => 'slack',
            'layout' => 'side-menu',
            'page_name' => 'dewey-classification',
            'title' => 'Dewey Classification'
        ],
           'devider',

            'tools' => [
                'icon' => 'codesandbox',
                'page_name' => 'tools',
                'title' => 'Tools',
                'sub_menu' => [
                    'schools' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'schools',
                        'title' => 'Schools'
                    ],
                    'fields' => [
                        'icon' => '',
                        'layout' => 'side-menu',
                        'page_name' => 'fields',
                        'title' => 'Fields'
                    ],
                    'authors' => [
                        'icon' => 'user',
                        'layout' => 'side-menu',
                        'page_name' => 'authors',
                        'title' => 'Authors'
                    ],
                    'publishers' => [
                        'icon' => 'user',
                        'layout' => 'side-menu',
                        'page_name' => 'publishers',
                        'title' => 'Publishers'
                    ]
                ]
            ],
          
        ];
    }
   
}
