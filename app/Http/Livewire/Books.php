<?php

namespace App\Http\Livewire;

use App\Models\Book;
use App\Models\Author;
use Livewire\Component;
use App\Models\BookType;
use App\Models\Publisher;
use App\Models\AuthorWork;
use App\Models\BookItem;
use App\Models\DDCInteger;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class Books extends Component
{
    use WithFileUploads;

    public $book_id;
    public $title;
    public $number_of_pages;
    public $cover;
    public $url;
    public $new_url;
    public $description;
    public $author_1;
    public $author_2;
    public $ISBN; 
    public $ISSN;
    public $type;
    public $ddc_integer_id;
    public $ddc_decimal_id=000;
    public $language;
    public $publisher;
    public $cat1_val;
    public $cat2_val;
    public $cat3_val;
    public $data;

    public $showModalForm = false;
    public $showDeleteModalForm = false;
    public $createAuthor = False;
    public $createPublisher = False;

    protected $listeners = [
        'author_1' => 'author_1',
        'author_2' => 'author_2',
        'publisher' => 'publisher',
        'createModel' => 'createModel',
        'dontCreateModel' => 'dontCreateModel',
        'showEditModal' => 'showEditModal',
        'showDeleteModal' => 'showDeleteModal'
    ];


    public function author_1(Author $author)
    {
        $this->author_1 = $author->name;
     
    }

    public function author_2(Author $author)
    {
        $this->author_2 = $author->name;
    }

    public function publisher(Publisher $publisher)
    {
        $this->publisher = $publisher->name;
    }

    public function createModel($data){
        $this->data = $data;
        if ($data[0] == "author") {
            $this->createAuthor = True;
        }else if ($data[0] == "publisher") {
            $this->createPublisher = True;
        }
    }

    public function dontCreateModel(){
        $this->createPublisher = False;
        $this->createAuthor = False;
    }

    public function createPublisher(){
        Publisher::create(["name" => $this->data[1]]);
        $this->publisher = $this->data[1];
        $this->alert('success', 'Publisher '.$this->data[1].' created', [
            'position' =>  'top-end', 
            'timer' =>  '9000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }
    public function createAuthor(){
        Author::create(["name" => $this->data[1]]);
        $this->author_1 = $this->data[1];
        $this->alert('success', 'Author '.$this->data[1].' created', [
            'position' =>  'top-end', 
            'timer' =>  '9000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

    public function updatingISBN($value)
    {
        if(strlen($value) >= 10){
            $this->callAPI($value); 
        }
    }

    public function callAPI($ISBN){
        try {
            $response = json_decode(file_get_contents('https://openlibrary.org/api/books?bibkeys=ISBN:'.$ISBN.'&jscmd=details&format=json'), true);
         
            if($response){
                $data = $response["ISBN:$ISBN"];  
    
                //Details of the book
                $details = $data["details"]; 
    
                //number_of_pages
                if($details["number_of_pages"]){
                    $this->number_of_pages = $details["number_of_pages"];
                }
    
                //title
                 if($details["title"]){
                    $this->title = $details["title"];
                }
    
                //description
                if(isset($details["description"])){
                    if(isset($details["description"]["value"])){
                        $this->description = $details["description"]["value"];
                    }else{
                        $this->description = $details["description"];
                    }
                }
                
                //authors
                if($details["authors"][0]){
                    $author = Author::where('name', 'like','%'.$details["authors"][0]["name"].'%')->first();
                    if(empty($author)){
                        Author::create(["name" => $details["authors"][0]["name"]]);
                    }
                    $this->author_1 = $details["authors"][0]["name"];
                    $this->emit('updateAuthor1Search',$author->id);
       
                }
                if(!empty($details["authors"][1])){
                    $author = Author::where('name', 'like','%'.$details["authors"][1]["name"].'%')->first();
                    if(empty($author)){
                        Author::create(["name" => $details["authors"][1]["name"]]);
                    }
                    $this->author_2 = $details["authors"][1]["name"];
                }
    
                //language
                 if($details["languages"][0]['key']){
                    $this->language = $details["languages"][0]['key'];
                }
    
                //publishers
                 if($details["publishers"][0]){
                    $publisher = Publisher::where('name', 'like','%'.$details["publishers"][0].'%')->first();
                    if(empty($publisher)){
                        $publisher =  Publisher::create(["name" => $details["publishers"][0]]);
                    }
                    $this->publisher = $details["publishers"][0];
                    $this->emit('updatePublisherSearch', $publisher->id);
                }
    
                //type
                if($details["type"]["key"]){
                    //$this->type = $details["type"]["key"];
                }
    
                //Dewey Classification
                if($details["dewey_decimal_class"][0]){
                   // dd($details["dewey_decimal_class"][0]);
                    $ddc = explode("/",$details["dewey_decimal_class"][0]);
                    $ddc_integer = $ddc[0];
                    $ddc_decimal = str_replace(".","",$ddc[1]);

                    if(Str::length($ddc_integer) == 1){
                        $ddc_integer.="00";
                    }elseif(Str::length($ddc_integer) == 2){
                        $ddc_integer.="0";
                    }

                    $this->ddc_integer_id = $ddc_integer;

                    if(Str::length($ddc_decimal) == 1){
                        $ddc_decimal.="00";
                    }elseif(Str::length($ddc_decimal) == 2){
                        $ddc_decimal.="0";
                    }

                    $this->ddc_decimal_id = $ddc_decimal;
               
                }
    
                $this->alert('success', 'Data received Successfully', [
                    'position' =>  'top-end', 
                    'timer' =>  '9000', 
                    'toast' =>  true, 
                    'text' =>  '', 
                    'confirmButtonText' =>  'Ok', 
                    'cancelButtonText' =>  'Cancel', 
                    'showCancelButton' =>  false, 
                    'showConfirmButton' =>  false, 
                ]);
            }else{
                $this->showCreateModal();
                $this->alert('error', 'We don\'t have the complete data for the ISBN : '.$ISBN.' on the API server', [
                    'position' =>  'top-end', 
                    'timer' =>  '9000', 
                    'toast' =>  true, 
                    'text' =>  '', 
                    'confirmButtonText' =>  'Ok', 
                    'cancelButtonText' =>  'Cancel', 
                    'showCancelButton' =>  false, 
                    'showConfirmButton' =>  false, 
                ]);
    
           }
        } catch (\Throwable $th) {
            $this->alert('error', 'An Error Occurred on the API server', [
                'position' =>  'top-end', 
                'timer' =>  '6000', 
                'toast' =>  true, 
                'text' =>  '', 
                'confirmButtonText' =>  'Ok', 
                'cancelButtonText' =>  'Cancel', 
                'showCancelButton' =>  false, 
                'showConfirmButton' =>  false, 
            ]);
        }
      
    }

    public function showCreateModal()
    {
        $this->emit('resetInputs');
        $this->reset();
        $this->resetValidation();
        $this->showModalForm = true;

    }

    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }

    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function storeBook()
    {
     
        $this->validate([
            'title' => ['required', 'string', 'max:255'],
            'type' => ['required'],
            'cat1_val'=> ['required'],
            'author_1' => ['required', 'string', 'max:255'],
           ]);
        $cover_name = "";
      
        if ($this->cover) {
            $cover_extension = $this->cover->getClientOriginalExtension();
            $cover_name = 'books'.'/'.Str::random(40).'.'.$cover_extension;
            $this->cover->storeAs('public/',  $cover_name);
        }else{
            $cover_name = "images/awaiting_cover.jpg";
        }
    
        if ($this->url) {
            $url_extension = $this->url->getClientOriginalExtension();
            $url_name = 'books'.'/'.Str::random(40).'.'.$url_extension;
            $this->url->storeAs('public/', $url_name);
        }

        if(!$this->ISSN){
            $this->ISSN = '';
        }
        if(!$this->ISBN){
            $this->ISBN = '';
        }
        $publisher= Publisher::where('name', 'like','%'.$this->publisher.'%')->orwhere('id', $this->publisher)->first();
        $author_1= Author::where('name', 'like','%'.$this->author_1.'%')->first();

        if(!empty($this->cat1_val) && !empty($this->cat2_val) && !empty($this->cat3_val)){
            $ddc = DDCInteger::where('integer_code', $this->cat3_val)->first();

        }else if(!empty($this->cat1_val) && !empty($this->cat2_val) && empty($this->cat3_val)){
            $ddc = DDCInteger::where('integer_code', $this->cat2_val)->first();

        }else if(!empty($this->cat1_val) && empty($this->cat2_val) && empty($this->cat3_val)){
            $ddc = DDCInteger::where('integer_code', $this->cat1_val)->first();
        }
    
        $book = Book::create([
            'title' => $this->title,
            'book_type_id' => $this->type,
            'ISBN' => $this->ISBN,
            'ISSN' => $this->ISSN,
            'cover' => $cover_name,
            'description' => $this->description,
            'author_1' => $author_1->id,
            'number_of_pages' => $this->number_of_pages,
            'language' =>  $this->language,
            'ddc_code' => $ddc->integer_code,
            'ddc_natural_id' => $ddc->id,
            'publisher_id' => $publisher->id,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
         ]);
        
      
        if ($this->author_1) {
            AuthorWork::create([
                'author_work_id' => $book->id,
                'author_work_type' => Book::class,
                'author_id' => $author_1->id,
                'status'  => true,
             ]);
        }
        if ($this->author_2) {
            AuthorWork::create([
                'author_work_id' => $book->id,
                'author_work_type' => Book::class,
                'author_id' => $author_1->id,
                'status'  => false,
             ]);
        }

        $this->alert('success', 'Book '.$this->title.' created Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);

        $this->emit('newQuery');
        $this->reset();
    }

    public function updateBook()
    {
        $this->validate([
            'title' => ['required', 'string', 'max:255'],
            'author_1' => ['required'],
            ]);

      if ($this->url) {
        Storage::delete('public/', $this->new_url);
        $this->new_url = 'books'.'/'.Str::random(40).'.'.$this->url->getClientOriginalExtension();
        $this->url->storeAs('public/', $this->new_url);
    }
    $cover_name = $this->cover;
    if ($this->cover && !is_string($this->cover)) {
           $cover =  Book::find($this->book_id)->first()->cover;
            Storage::delete('public/'.$cover);
            $cover_extension = $this->cover->getClientOriginalExtension();
            $cover_name = 'books'.'/'.Str::random(40).'.'.$cover_extension;
            $this->cover->storeAs('public/',  $cover_name);
    }
        if(!$this->ISSN){
            $this->ISSN = '';
        }
        if(!$this->ISBN){
            $this->ISBN = '';
        }
        Book::find($this->book_id)->update([
            'title' => $this->title,
            'book_type_id' => $this->type,
            'ISBN' => $this->ISBN,
            'ISSN' => $this->ISSN,
            'cover' => $cover_name,
            'updated_by' => Auth::user()->id,
        ]);
        $this->alert('success', 'The Book '.$this->title.' was updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
        
        $this->emit('updateQuery');
        $this->reset();
    }

    public function showEditModal($id)
    {
        $this->resetValidation();
        $this->reset();
        $this->showModalForm = true;
        $this->book_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $book = Book::findOrFail($this->book_id);
        $this->title =  $book->title;
        $this->type =  $book->book_type_id;
        $this->ISBN =  $book->ISBN;
        $this->ISSN =  $book->ISSN;
        $this->language =  $book->language;
        $this->description =  $book->description;
        if ($book->ddc_code) {
            $this->ddc_integer_id = explode(".",$book->ddc_code)[0];
         }
        $this->number_of_pages =  $book->number_of_pages;

        if(!empty($book->publisher)){
            $this->publisher =  $book->publisher->name;
            $this->emit('updatePublisherSearch',$book->publisher->id);
        }

        if(!empty($book->authors[0])){
            $this->author_1 =  $book->authors[0]->name;
            $this->emit('updateAuthor1Search', $book->authors[0]->id);
        }

        if(!empty($book->authors[1])){
            $this->author_2 =  $book->authors[1]->id;
            $this->emit('updateAuthor2Search', $book->authors[1]->id);
        }
        $this->cover = $book->cover;
    }

    public function showDeleteModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->book_id = $id;
    }

    public function deleteBook($id)
    {
        $book = Book::where('id', $id)->with('authors')->with('bookItems', function($query){
            return $query->with('bookings')->get();
        })->first();

        if($book->cover){
            Storage::delete('public/'.$book->cover);
        }

        foreach ($book->bookItems as $bookItem){
            if ($bookItem->url) {
                Storage::delete('public/'.$bookItem->url);
            }
            $bookItem->bookings()->delete();
        }
        $book->authors()->delete();
        $book->bookItems()->delete();
        $book->delete();
        $this->reset();
        $this->showDeleteModalForm = false;
        $this->alert('success', 'Book '.$book->title.' deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);

        $this->emit('deleteQuery');
    }

    public function render()
    {
        $types = BookType::all();
        $ddc_integers = DDCInteger::all();
        $cat1 = DDCInteger::where('integer_code', 'like', '_00')->get();
        $cat2 = DDCInteger::where('integer_code', 'like', Str::substr($this->cat1_val, 0, 1).'_0')->where('integer_code', '!=', Str::substr($this->cat1_val, 0, 1).'00')->get();
        $cat3 = DDCInteger::where('integer_code', 'like', Str::substr($this->cat2_val, 0, 2).'_')->where('integer_code', '!=', Str::substr($this->cat2_val, 0, 2).'0')->get();
        $authors = Author::all();
        $publishers = Publisher::all();
       
        return view('livewire.books', [
            'authors' => $authors,
            'types' => $types,
            'publishers' => $publishers,
            'cat1' => $cat1,
            'cat2' => $cat2,
            'cat3' => $cat3,
            ]);
    }
}
