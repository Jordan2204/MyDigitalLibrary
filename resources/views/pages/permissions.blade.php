@extends('../layouts/' . $layout)

@section('subhead')
    <title>Permissions Management</title>
@endsection

@section('subcontent')
    @livewire('permissions')
@endsection