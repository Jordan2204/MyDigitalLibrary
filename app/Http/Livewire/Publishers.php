<?php

namespace App\Http\Livewire;

use App\Models\Publisher;
use Livewire\Component;

class Publishers extends Component
{
    public $name;
    public $description;
    public $publisher_id;
    public $showModalForm = false;
    public $showDeleteModalForm = false;
   
    public function showCreatePublisherModal()
    {
        $this->resetValidation();
        $this->reset();
        $this->showModalForm = true;
    }

    public function closeModal()
    {
        $this->reset();
     }

    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function clickAway(){
        dd("");
    }

    public function storePublisher()
    {
        $this->validate([
          'name' =>'required'
      ]);
        $publisher = Publisher::create(['name' => $this->name]);
        $this->alert('success', 'Publisher '.$this->name.' created Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
      ]);
      $this->reset();
       
    }
    public function updatePublisher()
    {
        $this->validate([
          'name' =>'required',
      ]);

        Publisher::find($this->publisher_id)->update([
            'name' => $this->name,
            'description' => $this->description
        ]);
        $this->alert('success', 'Publisher '.$this->name.' updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
      ]);
      $this->reset();
      
    }

    public function showEditPublisherModal($id)
    {
        $this->reset();
        $this->showModalForm = true;
        $this->publisher_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $publisher = Publisher::findOrFail($this->publisher_id);
        $this->name = $publisher->name;
        $this->description = $publisher->description;
    }

    public function showDeletePublisherModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->publisher_id = $id;
    }

    public function deletePublisher($id)
    {
        $publisher = Publisher::find($id);
        $publisher->delete();
        $this->showDeleteModalForm = false;
        $this->alert('success', 'Publisher '.$publisher->name.' deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
      ]);
    }

    public function render()
    {
     
        return view('livewire.publishers', ['publishers' => Publisher::all()]);
    }
}
