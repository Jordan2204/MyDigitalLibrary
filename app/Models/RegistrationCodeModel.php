<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistrationCodeModel extends Model
{
    use HasFactory;

    protected $table= 'registration_codes';
    protected $guarded = ['created_at', 'updated_at'];

    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }
}
