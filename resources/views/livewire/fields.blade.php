<div class="container mx-auto mt-2">
    <div>
        <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ __('Field Management') }}</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <x-jet-button wire:click="showCreateModal" class=" mr-2 text-center bg-blue-800">{{__("Add a New Field") }}</x-jet-button>
            <div class="dropdown relative ml-auto sm:ml-0">
                <button class="dropdown-toggle button px-2 box text-gray-700">
                    <span class="w-5 h-5 flex items-center justify-center">
                        <i class="w-4 h-4" data-feather="plus"></i>
                    </span>
                </button>
            </div>
        </div>
    </div>

<!-- BEGIN: Datatable -->
<div class="intro-y datatable-wrapper box p-5 mt-5">
    @livewire('data-table.fields-table')
</div>
<!-- END: Datatable -->


<!-- Modal create Permission-->
<x-jet-dialog-modal class="z-40" wire:model="showModalForm">
        @if($field_id)
        <x-slot name="ico">
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-2 ml-2 h-6 w-6 text-yellow-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
            </svg>
        </x-slot>
        <x-slot name="title">{{__("Updating of a Field")}}</x-slot>   
    @else
        <x-slot name="ico">
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-2 ml-2 h-6 w-6 text-green-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
        </x-slot>
        <x-slot name="title">{{ __("Creation of a Field") }}</x-slot>  
    @endif
    <x-slot name="close">
        <a x-on:click.prevent @click="@this.closeModal()" href=""> 
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-1 ml-2 text-white h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </a> 
    </x-slot>
     <x-slot name="content">
       <div class="space-y-8 divide-y divide-gray-200">
        @if($field_id)
        <form id="form_create" wire:submit.prevent="update" autocomplete="off">
        @else
        <form id="form_create" wire:submit.prevent="store" autocomplete="off">
        @endif  
                <div class="mt-2 sm:col-span-12">
                    <label for="name" class="block text-sm font-medium text-gray-700"> {{__("Title")}} </label>
                    <div class="mt-1">
                        <input type="text" id="title" wire:model.lazy="title" name="title" class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                    </div>
                    @error('title') <span class="error text-red-600">{{ $message }}</span> @enderror
                </div>
                <div class="mt-2 sm:col-span-12">
                    <label for="name" class="block text-sm font-medium text-gray-700"> {{__("Abbreviation")}} </label>
                    <div class="mt-1">
                        <input type="text" id="abbr" wire:model.lazy="abbr" name="abbr" class="block w-full appearance-none bg-white border border-gray-400 rounded-md py-2 px-3 text-base leading-normal transition duration-150 ease-in-out sm:text-sm sm:leading-5" />
                    </div>
                    @error('abbr') <span class="error text-red-600">{{ $message }}</span> @enderror
                </div>

                <div class="mt-4 sm:col-span-12">
                    <x-jet-label for="name" value="{{ __('Description') }}" />
                    <div wire:ignore class="form-group">
                        <textarea id="description_text" wire:model.defer="description" class="summernote form-control">{{ $description }}</textarea>
                    </div>
                    @error('description') <span class="error text-red-600">{{ $message }}</span> @enderror
                </div>
          
        </div>
     </x-slot>

     <x-slot name="footer">
        @if($field_id)
            <x-jet-button type="submit">{{ __('Update') }}</x-jet-button>
            @else
            <x-jet-button type="submit">{{ __('Create') }}</x-jet-button>
        @endif
     </x-slot>
    </form>
 </x-jet-dialog-modal>


    <x-jet-confirmation-modal wire:model="showDeleteModalForm">
    <x-slot name="title">{{ __("Delete Fields") }}</x-slot>
    <x-slot name="close">
        <a x-on:click.prevent @click="@this.closeModal()" href=""> 
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-1 ml-2 text-white h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </a> 
    </x-slot>
    <x-slot name="content">
        <div class="space-y-8 text-2xl divide-y divide-gray-200">
            <div class="p-5 text-center"> <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                @if($this->field_id)
                <p>{{__("Do you really want to delete this") }} <span class="text-gray-900 text-bold">{{__("Data")}} ?</span> {{ __("his process cannot be undone")}}.</p>
                @endif
            </div>      
        </div>
    </x-slot>

       <x-slot name="footer">
           <x-jet-button wire:click="delete({{ $this->field_id }})" class="bg-red-700"> {{ __('Delete')}}</x-jet-button>
       </x-slot>
   </x-jet-confirmation-modal>
</div>

@push('js')
  <script>
        $('#form_create').submit(function() {
            console.log($('#description_text').val());
            @this.set('description', $('#description_text').val());
         });
    </script>
@endpush