<?php

namespace App\Http\Livewire;

use Livewire\Component;

abstract class Autocomplete extends Component
{
    public $var_name;
    public $model_name;
    public $results;
    public $search;
    public $selected;
    public $showDropdown;
    abstract public function query();

    public function mount()
    {
        $this->showDropdown = false;
        $this->results = collect();
    }

    public function updatedSelected()
    {
        $this->emit($this->var_name, ['id' => $this->selected,'name' => $this->search]);
    }

    public function updatedSearch()
    {
        if (strlen($this->search) < 2) {
            $this->results = collect();
            $this->showDropdown = false;
            return;
        }

        if ($this->query()) {
            $this->results = $this->query()->get();
             if(empty($this->results[0])){
                $this->emit('createModel',[$this->model_name, $this->search]);
            }else{
                $this->emit('dontCreateModel');
            }
        } else {
            $this->results = collect();
        }

        $this->selected = '';
        $this->showDropdown = true;
    }

    public function render()
    {
        return view('livewire.autocomplete');
    }
}