@component('mail::message')
# Booking Cancelled <strong>ID : {{ $details['reservation_id'] }} </strong>

Your booking has been cancelled because you have not respected the issue date to pick up the book which was the <strong>{{ $details['issue_date'] }} </strong>

@component('mail::button', ['url' => 'http://mydigitallibrary.test'])
Visit our Digital Library.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
