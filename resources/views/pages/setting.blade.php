@extends('../layouts/' . $layout)

@section('subhead')
    <title>Home Settings</title>
@endsection

@section('subcontent')
   @livewire('settings')
@endsection