<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('schools')->insert([
            'title' => "",
            'abbr' => "ISTDI",
        ]);

        DB::table('schools')->insert([
            'title' => "",
            'abbr' => "SEAS",
        ]);

        DB::table('schools')->insert([
            'title' => "",
            'abbr' => "3IAC",
        ]);

        DB::table('schools')->insert([
            'title' => "",
            'abbr' => "3IL",
        ]);
    }
}
