@extends('../layouts/' . $layout)

@section('subhead')
    <title>Work Management</title>
@endsection

@section('subcontent')
   @livewire('subjects')
@endsection