<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model{
    protected $guarded = ['created_at','updated_at'];
    protected $table= 'medias';

    public function mediable(){
       return $this->morphTo(__FUNCTION__,'media_type','media_id'); //
    }
}
