<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('book_types')->delete();

        // DB::table('book_types')->insert([
        //     'title' => "Report",
        // ]);

        // DB::table('book_types')->insert([
        //     'title' => "Subject",
        // ]);

        // DB::table('book_types')->insert([
        //     'title' => "Book",
        // ]);

        // DB::table('book_types')->insert([
        //     'title' => "Magazine",
        // ]);

        // DB::table('book_types')->insert([
        //     'title' => "Journal",
        // ]);  

        // DB::table('book_types')->insert([
        //     'title' => "Review",
        // ]);  


        ///Status
        DB::table('status')->insert([
            'title' => "Available",
            'slug' => "available",
            'style_css' => "",
            'colour_class' => "green-600",
        ]);  

        DB::table('status')->insert([
            'title' => "Unavailable",
            'slug' => "unavailable",
            'style_css' => "",
            'colour_class' => "red-600",
        ]);  

        DB::table('status')->insert([
            'title' => "Used",
            'slug' => "used",
            'style_css' => "",
            'colour_class' => "red-600",
        ]);  

        DB::table('status')->insert([
            'title' => "Active",
            'slug' => "active",
            'style_css' => "",
            'colour_class' => "green-600",
        ]);  
    }
}
