<?php

namespace App\Http\Livewire;

use App\Models\Field;
use Livewire\Component;

class Fields extends Component
{
    public $field_id;
    public $title;
    public $abbr;
    public $description;
    public $showModalForm = false;
    public $showDeleteModalForm = false;

    protected $listeners = [
        'showEditModal' => 'showEditModal',
        'showDeleteModal' => 'showDeleteModal'
    ];


    public function showCreateModal()
    {
        $this->resetValidation();
        $this->showModalForm = true;
    }
    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }

    public function store()
    {
        $this->validate([
            'title' =>'required',
            'abbr' =>'required'
        ]);

        $field = Field::create([
            'title' => $this->title,
            'abbr' => $this->abbr,
            'description' => $this->description
        ]);
        $this->reset();
        $this->emit('newQuery');
        //session()->flash('flash.banner', 'Permission created Successfully');
    }
    public function update()
    {
        $this->validate([
            'title' =>'required',
            'abbr' =>'required'
      ]);

        Field::find($this->field_id)->update([
            'title' => $this->title,
            'abbr' => $this->abbr,
            'description' => $this->description
        ]);
        $this->reset();
        $this->emit('updateQuery');
        //session()->flash('flash.banner', 'Post Updated Successfully');
    }

    public function showEditModal($id)
    {
        $this->reset();
        $this->showModalForm = true;
        $this->field_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $field = Field::findOrFail($this->field_id);
        $this->title = $field->title;
        $this->abbr = $field->abbr;
        $this->description = $field->description;
    }

    public function showDeleteModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->field_id = $id;
    }

    public function delete($id)
    {
        $field = Field::find($id);
        $field->delete();
        $this->showDeleteModalForm = false;
        $this->emit('deleteQuery');
     }
     
    public function render()
    {
        return view('livewire.fields');
    }
}
