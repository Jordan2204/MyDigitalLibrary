<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    use HasFactory;

    protected $fillable = [
        'mark_id',
        'mark_type',
        'user_id',
        'value',
    ];

    public function mark(){
        return $this->morphTo(__FUNCTION__,'mark_type','mark_id');
    }
}

