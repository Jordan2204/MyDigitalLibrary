<?php

namespace App\Http\Livewire\DataTable;

use App\Models\Book;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class BooksTable extends LivewireDatatable
{
    public $model = Book::class;

    public $hideable = 'select';
    public $exportable = true;
    
    protected $listeners =
     [
        'newQuery' => 'refresh_data',
        'updateQuery' => 'refresh_data',
        'deleteQuery' => 'refresh_data'
    ];

    public function builder()
    {
        return  Book::query();
    }

    public function showEditModal($id){
        $this->emitUp('showEditModal', $id);
    }

    public function showDeleteModal($id){
        $this->emitUp('showDeleteModal', $id);
    }

    
    public function refresh_data()
    {
        $this->emit('updateSavedQueries');
    }

  
    public function columns()
    {
        return [
           
            Column::callback(['id', 'cover'], function ($id, $cover) {
                return view('datatables.table-cover', 
                ['id' => $id, 'cover' => $cover]);
            })->label(__('Cover'))
            ->unsortable(),

            Column::name('title')
                ->label(__('Title'))
                ->searchable()
                ->alignCenter(),

            Column::name('authors.name')
                ->label(__('Author'))
                ->searchable(),
            
            Column::name('type.title')
            ->label('Type')
            ->searchable(),

            Column::callback(['id', 'title'], function ($id, $title) {
                return view('datatables.table-actions', 
                ['id' => $id,'type' => 'book', 'route' => 'book-items.index', 'data' => ['id' => $id]]);
            })->label('Actions')
            ->unsortable()
           

        ];
    }
}