@extends('../layouts/' . $layout)

@section('subhead')
    <title>{{__('Work Management') }}</title>
@endsection

@section('subcontent')
   @livewire('student-reports')
@endsection