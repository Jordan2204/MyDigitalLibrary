<?php

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{
    /**
     * @param  $request
     * @return mixed
     */
    public function toResponse($request)
    {
        //Save data into the session
        $user = auth()->user();
        $auth = $user ? true : false;
        $roles = $user->getRoleNames();
        $isSuperAdmin = $user->hasRole('SuperAdmin');
        $isAdmin = $user->hasRole('Admin');
        $isLibrarian = $user->hasRole('Librarian');
        $isMember = $user->hasRole('Member');
        $isStudent = $user->hasRole('Student');
        $isLecturer = $user->hasRole('Lecturer');
     
        session([
            'user' => $user,
            'auth' => $auth,
            'roles' => $roles,
            'isSuperAdmin' => $isSuperAdmin,
            'isAdmin' => $isAdmin,
            'isLibrarian' => $isLibrarian,
            'isMember' => $isMember,
            'isStudent' => $isStudent,
            'isLecturer' => $isLecturer
        ]);

    	//Redirect after login.
       	$home = '/';

        return redirect()->intended($home);
    }
}