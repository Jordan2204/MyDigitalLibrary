<?php

namespace App\Models;

use App\Models\City;
use App\Models\Media;
use App\Models\Country;
use App\Models\District;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Laravel\Jetstream\HasProfilePhoto;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable{
    use HasRoles;
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    protected $table ='users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['created_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function likes()
    {
        return $this->hasMany(Like::class, 'user_id');
    }

    public function ratings()
    {
        return $this->hasMany(Mark::class, 'user_id');
    }

    
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    /*return the views of a book*/
    public function views(){
        return $this->hasMany(View::class,'user_id');
    }


}
