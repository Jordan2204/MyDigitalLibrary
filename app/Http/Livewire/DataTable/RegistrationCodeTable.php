<?php

namespace App\Http\Livewire\DataTable;

use App\Models\RegistrationCodeModel;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class RegistrationCodeTable extends LivewireDatatable
{
    public $model = RegistrationCodeModel::class;

    public $hideable = 'select';
    public $exportable = true;
    
    protected $listeners =
     [
        'newQuery' => 'refresh_data',
        'updateQuery' => 'refresh_data',
        'deleteQuery' => 'refresh_data'
    ];

    public function builder()
    {
        return  RegistrationCodeModel::query();
    }

    public function showEditModal($id){
        $this->emitUp('showEditModal', $id);
    }

    public function showDeleteModal($id){
        $this->emitUp('showDeleteModal', $id);
    }

    
    public function refresh_data()
    {
        $this->emit('updateSavedQueries');
    }

  
    public function columns()
    {
        return [
           
            Column::name('code')
                ->label(__('Code'))
                ->searchable()
                ->alignCenter(),

            Column::name('role')
            ->label(__('Role'))
            ->searchable()
            ->alignCenter(),


            Column::name('status.title')
                ->label(__('Status'))
                ->searchable(),

            Column::callback(['id', 'code'], function ($id, $code) {
                return view('datatables.table-actions', 
                ['id' => $id, 'type' => null]);
            })->label('Actions')
            ->unsortable()
           

        ];
    }
}