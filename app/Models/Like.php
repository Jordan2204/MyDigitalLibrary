<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'like_id',
        'like_type',
        'user_id',
    ];

    public function like(){
        return $this->morphTo(__FUNCTION__,'like_type','like_id');
    }
}
