<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Models\Author;
use App\Models\Field;
use App\Models\Level;
use App\Models\Period;
use App\Models\School;
use App\Models\Subject;
use Illuminate\Support\Facades\Auth;

class Subjects extends Component
{
    use WithFileUploads;

    public $subject_id;
    public $title;
    public $level;
    public $school;
    public $period;
    public $cover;
    public $url;
    public $new_url;
    public $field;
    public $author;
    public $academic_year;
    
    public $showModalForm = false;
    public $showDeleteModalForm = false;

    protected $listeners = ['showEditModal', 'showDeleteModal'];


    public function showCreateModal()
    {
        $this->emit('resetInputs');
        $this->resetValidation();
        $this->reset();
        $this->showModalForm = true;
    }

    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }
    
    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function store()
    {
        $this->validate([
            'title' => ['required', 'string', 'max:255'],
            'author' => ['required'],
            'url' => ['required','file', 'mimes:pdf', 'max:50000'],
            'level' => ['required'],
            'school' => ['required'],
            'period' => ['required'],
            'field' => ['required'],
            'academic_year' => ['required'],
            ]);

        $cover_name = '';
        $url_name = '';
        if ($this->url) {
            $url_extension = $this->url->getClientOriginalExtension();
            $url_name = 'subjects'.'/'.Str::random(40).'.'.$url_extension;
            $this->url->storeAs('public/', $url_name);
            $url_name = $url_name;

            if ($this->cover) {
                $cover_extension = $this->cover->getClientOriginalExtension();
                $cover_name = 'subjects'.'/'.Str::random(40).'.'.$cover_extension;
                $this->cover->storeAs('public/',  $cover_name);
            }else{
                //Path
                $base_path = base_path();
                $base_final_path = str_replace("\\", "/", $base_path);
                $cover_name = 'subjects'.'/'.Str::random(40).'.png';
        
                $Imagick = new \Imagick();
                $Imagick->readImage($base_final_path.'/public/storage/'.$url_name.'[0]');  
                $Imagick->setImageFormat('png');
                $Imagick->writeImage($base_final_path.'/public/storage/'.$cover_name);

            }
            
        }
      
        $subject = Subject::create([
            'title' => $this->title,
            'academic_year' => $this->academic_year,
            'url' => $url_name,
            'cover' => $cover_name,
            'level_id' => $this->level,
            'field_id' => $this->field,
            'period_id' => $this->period,
            'author_id' => $this->author,
            'school_id' => $this->school,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
        ]);
        $this->alert('success', 'The Subject '.$this->title.' was created Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);

        $this->reset();
        $this->emit('newQuery');

    }
    public function update()
    {
        $this->validate([
            'title' => ['required', 'string', 'max:255'],
            ]);


    $cover_name = $this->cover;
    $url_name = $this->url;

    if(!is_string($this->url)) {
        //dd($this->new_url.'     '.$this->cover);
        Storage::delete('public/', $this->new_url);
        Storage::delete('public/', $this->cover);
       
        $url_extension = $this->url->getClientOriginalExtension();
        $url_name = 'subjects'.'/'.Str::random(40).'.'.$url_extension;
        $this->url->storeAs('public/', $url_name);
        $url_name = $url_name;

        if ($this->cover) {
            $cover_extension = $this->cover->getClientOriginalExtension();
            $cover_name = 'subjects'.'/'.Str::random(40).'.'.$cover_extension;
            $this->cover->storeAs('public/',  $cover_name);
        }else{
            //Path
            $base_path = base_path();
            $base_final_path = str_replace("\\", "/", $base_path);
            $cover_name = 'subjects'.'/'.Str::random(40).'.png';
    
            $Imagick = new \Imagick();
            $Imagick->readImage($base_final_path.'/public/storage'.$url_name.'[0]');
            $Imagick->setImageBackgroundColor('#ffffff');
            $Imagick->setImageAlphaChannel(\Imagick::ALPHACHANNEL_REMOVE);
            $Imagick = $Imagick->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN); 
            $Imagick->setImageFormat('png');
            $Imagick->writeImage($base_final_path.'/public/storage'.$cover_name);

        }
        
    }

        Subject::find($this->subject_id)->update([
            'title' => $this->title,
            'academic_year' => $this->academic_year,
            'url' => $url_name,
            'cover' => $cover_name,
            'level_id' => $this->level,
            'field_id' => $this->field,
            'period_id' => $this->period,
            'author_id' => $this->author,
            'updated_by' => Auth::user()->id,
      
        ]);

        $this->alert('success', 'The Subject '.$this->title.' was updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
        $this->reset();
        $this->emit('updateQuery');
  
     }

    public function showEditModal($id)
    {
        $this->resetValidation();
        $this->reset();
        $this->showModalForm = true;
        $this->subject_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $subject = Subject::findOrFail($this->subject_id);
        $this->title =  $subject->title;
        $this->author =  $subject->author->id;
        $this->academic_year =  $subject->academic_year;
        $this->cover = $subject->cover;
        $this->url = $subject->url;
        $this->field = $subject->field->id;
        $this->period = $subject->period->id;
        $this->level = $subject->level->id;
        $this->school = $subject->school->id;
    }

    public function showDeleteModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->subject_id = $id;
    }

    public function delete($id)
    {
        $subject = Subject::find($id);
       Storage::delete('public/'.$subject->url);
        Storage::delete('public/'.$subject->cover);
        $subject->delete();
        $this->reset();
       
        $this->showDeleteModalForm = false;
        $this->alert('success', 'Subject '.$subject->title.' deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);

        $this->emit('deleteQuery');
  
    }

    public function render()
    {
        $levels = Level::all();
        $schools = School::all();
        $authors = Author::all();
        $fields = Field::all();
        $periods = Period::all();
        return view('livewire.subjects', [
            'authors' => $authors,
            'levels' => $levels,
            'schools' => $schools,
            'fields' => $fields,
            'periods' => $periods
            ]);
    }
}
