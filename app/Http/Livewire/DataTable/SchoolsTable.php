<?php

namespace App\Http\Livewire\DataTable;

use App\Models\School;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class SchoolsTable extends LivewireDatatable
{
    public $model = School::class;

   
    public $hideable = 'select';
    public $exportable = true;
    
    protected $listeners =
     [
        'newQuery' => 'refresh_data',
        'updateQuery' => 'refresh_data',
        'deleteQuery' => 'refresh_data'
    ];

    public function builder()
    {
        return  School::query();
    }

    public function showEditModal($id){
        $this->emitUp('showEditModal', $id);
    }

    public function showDeleteModal($id){
        $this->emitUp('showDeleteModal', $id);
    }

    
    public function refresh_data()
    {
        $this->emit('updateSavedQueries');
    }

  
    public function columns()
    {
        return [

            Column::name('title')
                ->label(__('Title'))
                ->searchable()
                ->alignCenter(),
            
            Column::name('abbr')
            ->label(__('Abbreviation'))
            ->searchable()
            ->alignCenter(),

            Column::name('description')
            ->label(__('Description'))
            ->truncate(50)
            ->searchable(),

            Column::callback(['id', 'title'], function ($id, $title) {
                return view('datatables.table-actions', 
                ['id' => $id, 'name' => $title, 'type' => 'school']);
            })->label('Actions')
            ->unsortable()       

        ];
    }
}