<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\DDCInteger;

class DeweyClassification extends Component
{
    public $idToUpdate;
    public $integer_title;

    protected $listeners = [
        'edit_input' => 'edit_input',
        'update_input' => 'update'
    ];

    public function edit($id){
        $this->idToUpdate = $id;
        $this->integer_title =  DDCInteger::find($id)->first()->integer_title;
    }

    public function update($data){
        DDCInteger::find($data[0])->update([
            'integer_title' => $data[1],
       ]);
       $this->reset();
       $this->alert('success', 'Category updated Successfully', [
        'position' =>  'top-end', 
        'timer' =>  '9000', 
        'toast' =>  true, 
        'text' =>  '', 
        'confirmButtonText' =>  'Ok', 
        'cancelButtonText' =>  'Cancel', 
        'showCancelButton' =>  false, 
        'showConfirmButton' =>  false, 
    ]);
    $this->emit('newQuery');

    }
    public function render()
    {
        //$ddc_integers = DDCInteger::all();
        return view('livewire.dewey-classification');
    }
}
