<?php

namespace App\Http\Livewire;

use App\Models\Book;
use App\Models\Status;
use Livewire\Component;
use App\Models\BookItem;
use App\Models\Publisher;
use App\Models\BookFormat;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\PageController;

class BookItems extends Component
{
    use WithFileUploads;

    public $book;
    public $book_id;
    public $book_item_id;
    public $code;
    public $format;
    public $price;
    public $publish_date;
    public $publish_country;
    public $publish_city;
    public $date_of_purchase;
    public $url;
    public $status_id;
    public $showModalForm = false;
    public $showDeleteModalForm = false;
    public $showUrl = true;
    protected $pageController;

   
    public function updatedFormat(){
        if($this->format == 2){
            $this->showUrl = false;
        }else{
            $this->showUrl = true;
     
        }
    }
    public function callAPI(){
        $ISBN = $this->book->ISBN;
        $response = json_decode(file_get_contents('https://openlibrary.org/api/books?bibkeys=ISBN:'.$ISBN.'&jscmd=details&format=json'), true);
        if($response){
            $data = $response["ISBN:$ISBN"];  

            //Details of the book
            $details = $data["details"]; 
            
            //publish_country
           if($details['publish_country']){
               $this->publish_country = $details['publish_country'];
           }
            //publishers
             if($details["publishers"]){
                $publisher = Publisher::where('name', 'like','%'.$details["publishers"][0].'%')->first();
                if(empty($publisher)){
                    $publisher =  Publisher::create(["name" => $details["publishers"][0]]);
                }
                $this->publisher = $details["publishers"][0];
            }

            //publish_date
             if($details["publish_date"]){
                $this->publish_date = $details["publish_date"];
            }

        }else{
            $book_id = $this->book_id;
            $this->reset();
            $this->book_id = $book_id;
       }
    }

    public function showCreateBookItemModal()
    {
        $book_id = $this->book_id;
        $this->reset();
        $this->book_id = $book_id;
        $this->resetValidation();
        $this->showModalForm = true;
    }
    public function updatedShowModalForm()
    {
        $book_id = $this->book_id;
        $this->reset();
        $this->book_id = $book_id;
    }

    public function closeModal()
    {
        $book_id = $this->book_id;
        $this->reset();
        $this->book_id = $book_id;
        $this->showDeleteModalForm = false;
    }

    public function storeBookItem()
    {
        $this->validate([
            'code' => ['required', 'string', 'max:10'],
            'format' => ['required'],
            'publish_country' => ['required'],
            'publish_date' => ['required'],
            'status_id' => ['required'],
            ]);
    
        $url_name_file = "";
        if ($this->url) {
            $url_extension = $this->url->getClientOriginalExtension();
            $url_name = 'books'.'/'.Str::random(40).'.'.$url_extension;
            $url_name_file = $url_name;
            $this->url->storeAs('public/', $url_name);
      
            $base_path = base_path();
            $base_final_path = str_replace("\\", "/", $base_path);
            $cover_name = 'books'.'/'.Str::random(40).'.png';
    
            $Imagick = new \Imagick();
            $Imagick->readImage($base_final_path.'/public/storage'.'/'.$url_name.'[0]');
            $Imagick->setImageBackgroundColor('#ffffff');
            $Imagick->setImageAlphaChannel(\Imagick::ALPHACHANNEL_REMOVE);
            $Imagick = $Imagick->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN); 
            $Imagick->setImageFormat('png');
            $Imagick->writeImage($base_final_path.'/public/storage'.'/'.$cover_name);

            Book::find($this->book->id)->update([
                'cover' => $cover_name,
            ]);
        }
        $price = 0.0;
        if ($this->price) {
            $price = $this->price;
        }

        $book = BookItem::create([
            'code' => $this->code,
            'price' => $price,
            'date_of_purchase' => $this->date_of_purchase,
            'publish_date' => $this->publish_date,
            'publish_city' => $this->publish_city,
            'publish_country' => $this->publish_country,
            'book_format' => $this->format,
            'status_id' => $this->status_id,
            'book_id' => $this->book->id,
            'url' => $url_name_file,
           ]);
        
         $book_id = $this->book_id;
         $this->alert('success', 'The Book Item '.$this->book->title.' was created Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
         $this->reset();
         $this->book_id = $book_id;
    }

    public function updateBookItem()
    {
        $this->validate([
            'code' => ['required', 'string', 'max:20'],
            ]);

            $url_name_file = "";
            if ($this->url) {
                $url_extension = $this->url->getClientOriginalExtension();
                $url_name = 'books'.'/'.Str::random(40).'.'.$url_extension;
                $url_name_file = $url_name;
                $this->url->storeAs('public/', $url_name);
            }

        BookItem::find($this->book_item_id)->update([
            'code' => $this->code,
            'price' => $this->price,
            'date_of_purchase' => $this->date_of_purchase,
            'publish_date' => $this->publish_date,
            'publish_city' => $this->publish_city,
            'publish_country' => $this->publish_country,
            'book_format' => $this->format,
            'status_id' => $this->status_id,
            'book_id' => $this->book->id,
            'url' => $url_name_file,
        ]);

        $book_id = $this->book_id;
        $this->alert('success', 'The Book Item '.$this->book->title.' was updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
        $this->reset();
        $this->book_id = $book_id;
     }

    public function showEditBookItemModal($id)
    {
        $book_id = $this->book_id;
        $this->reset();
        $this->book_id = $book_id;
        $this->showModalForm = true;
        $this->book_item_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $book_item = BookItem::findOrFail($this->book_item_id);
        $this->code =  $book_item->code;
        $this->publish_country =  $book_item->publish_country;
        $this->publish_date =  $book_item->publish_date;
        $this->status_id =  $book_item->status_id;
        $this->price =  $book_item->price;
        $this->format =  $book_item->book_format;
    }

    public function showDeleteBookItemModal($id)
    {
        $book_id = $this->book_id;
        $this->reset();
        $this->book_id = $book_id;
        $this->showDeleteModalForm = true;
        $this->book_item_id = $id;
    }

    public function deleteBookItem($id)
    {
        $book_item = BookItem::find($id)->with('bookings')->first();
        if ($book_item->url) {
            Storage::delete('public/'.$book_item->url);
        }
        $book_item->bookings()->delete();
        $book_item->delete();
        $book_id = $this->book_id;
        $this->reset();
        $this->book_id = $book_id;
        $this->showDeleteModalForm = false;
        $this->alert('success', 'The Book Item was deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
     }

    public function render()
    {
        $this->book = Book::where('id', $this->book_id)->with('authors')->with('bookItems')->first();
        //$this->callAPI();
        $this->pageController = new PageController();
        $book_item_page  =  $this->pageController->loadPage("books");
        $book_items = $this->book->bookItems;
        $formats = BookFormat::all();
        $status = Status::all();
        return view('livewire.book-items', [
            'layout' => 'side-menu', 
            'side_menu' =>  $book_item_page->side_menu,
            'first_page_name' =>$book_item_page->first_page_name,
            'second_page_name' =>$book_item_page->second_page_name,
            'third_page_name' =>"Book-Items",
            'book_items' => $book_items,
            'formats' => $formats,
            'status' => $status
            ])->layout('layouts.side-menu',
            [ 
            'layout' => 'side-menu', 
            'side_menu' =>  $book_item_page->side_menu,
            'first_page_name' =>$book_item_page->first_page_name,
            'second_page_name' =>$book_item_page->second_page_name,
            'third_page_name' =>"Book-Items",  
            ]);
    }
}
