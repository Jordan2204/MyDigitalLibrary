<?php

namespace App\Http\Livewire;

use App\Mail\ReservationMail;
use App\Models\Book;
use App\Models\User;
use Livewire\Component;
use App\Models\Reservation;
use Illuminate\Support\Facades\Mail;

class Reservations extends Component
{
    public $reservation_id;
    public $reservation_detail;
    public $showModalForm = false;
    public $showDeleteModalForm = false;

    public function showCreatePublisherModal()
    {
        $this->resetValidation();
        $this->showModalForm = true;
    }

    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }

    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function showEditReservationModal(){

    }
    public function showDeleteReservationModal(){
        
    }

    public function confirmReservation($id){
        //Send email after confirming the reservation
       Reservation::find($id)->update([
            'status_id' => 3,
       ]);

        $reservation = Reservation::find($id);
        $user = User::where("id", $reservation->user_id)->first()->toArray();
        $book_item =  $reservation->book;
        $book = Book::find($book_item->book_id);
        $this->reservation_detail = [
            "issue_date" =>  $reservation->issue_date,
            "book_title" =>  $book->title,
            "book_item_code"  =>  $book_item->code,
            "reservation_id"  =>  $reservation->id,
         ];

        //To avoid waiting that the email is send before return the response
        dispatch(function () {
            Mail::to('anafackjordan@gmail.com')->send(new ReservationMail($this->reservation_detail));
        })->afterResponse();

        $this->alert('success', 'An email has been send to '.$user['name'].' to confirm the reservation', [
            'position' =>  'top-end', 
            'timer' =>  '9000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

    public function deleteReservation($id)
    {
        $reservation = Reservation::find($id);
        $reservation->delete();
        $this->showDeleteModalForm = false;
        //session()->flash('flash.banner', 'Role'. $role->name.' Deleted Successfully');
    }

    public function render()
    {
        $reservations = Reservation::with('book')->get();
        return view('livewire.reservations', [
            'reservations' => $reservations,
        ]);
    }
}
