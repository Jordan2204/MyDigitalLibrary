@if($id_to_update == $id)
    <input id="integer_title" name="integer_title" wire:model="integer_title" type="text" class="shadow appearance-none border rounded-md w-full py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" required autofocus autocomplete="integer_title">
@else
    <div class="font-medium whitespace-no-wrap"> {{ $integer_title }}</div>
@endif