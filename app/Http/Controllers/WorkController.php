<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Like;
use App\Models\Mark;
use App\Models\View;
use Inertia\Inertia;
use App\Models\Report;
use App\Models\Status;
use App\Models\Subject;
use App\Models\BookItem;
use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Redirect;

class WorkController extends Controller
{
    protected $sessionController;
    protected $perPage = 20;
    public function __construct(SessionController $sessionController){
        $this->sessionController = $sessionController;
    }

    public function fetch_works($page, $per_page){
        $books = Book::with('authors')->with('publisher')->with('format')->with('type')->orderBy('id', 'DESC')->get();
        $reports = Report::with('authors')->with('field')->with('school')->with('level')->orderBy('id', 'DESC')->get();
        $subjects = Subject::with('author')->with('field')->with('school')->with('period')->with('level')->orderBy('id', 'DESC')->get();

        $works = new \Illuminate\Database\Eloquent\Collection; 
        $works = $works->concat($books);
        $works = $works->concat($reports);
        $works = $works->concat($subjects);
        $works = $works->shuffle();
        $works = $works->slice((($page - 1)*$per_page), $per_page);
        $all_works = count($works);

        return json_encode([$works, $all_works]);
    }

    public function fetch_reports($page, $per_page){
        $reports = Report::with('authors')->with('field')->with('school')->with('level')->orderBy('id', 'DESC')->get();
        $works = $reports->slice((($page - 1)*$per_page), $per_page);
        return json_encode($works);
    }

    public function works($type="all", $search=""){
        $is_search = true;
        if($search == ""){
            $is_search = false;
        }
        $search = '%'.$search.'%';
        $works = null;
        $selected_type = null;
        if ($type == 'all') {

            $books = Book::where('title','like',$search)->with('authors')->withCount('views')->with('publisher')->with('format')->with('type')->withCount('likes')->orderBy('id', 'DESC')->get();
            $reports = Report::where('title','like',$search)->with('authors')->with('school')->withCount('views')->with('field')->with('level')->withCount('likes')->orderBy('id', 'DESC')->get();
            $subjects = Subject::where('title','like',$search)->with('author')->with('school')->withCount('views')->with('field')->with('period')->withCount('likes')->with('level')->orderBy('id', 'DESC')->get();
            $works = new \Illuminate\Database\Eloquent\Collection; 
            $works = $works->concat($books);
            $works = $works->concat($reports);
            $works = $works->concat($subjects);

        }elseif($type == 'reports'){
            $reports = Report::where('title','like',$search)->with('authors')->with('school')->withCount('likes')->withCount('views')->with('field')->with('level')->orderBy('id', 'DESC')->get();
            $works = $reports;
            $selected_type = "Report";
        }elseif($type == 'books'){
            $books = Book::where('title','like',$search)->with('authors')->withCount('likes')->withCount('views')->with('publisher')->with('format')->with('type')->orderBy('id', 'DESC')->get();
            $works = $books;
            $selected_type = "Book";
            
        }elseif($type == 'subjects'){
            $subjects = Subject::where('title','like',$search)->with('author')->with('school')->withCount('likes')->withCount('views')->with('field')->with('period')->with('level')->orderBy('id', 'DESC')->get();
            $works = $subjects;
            $selected_type = "Subject";
        }
        $works = $works->shuffle();
        $works = $works->slice(0, $this->perPage);
        return Inertia::render('Works/index', [
            'auth' => Auth::check(),
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
            'works' => $works,
            'selected_type' => $selected_type,
            'is_search' => $is_search,
            'session' => $this->sessionController->index()
       
        ]);
    }

    public function reservation(Request $request){
        $request->validate([
                'book_item_id' => 'required',
           
            ]);
        $reservation = Reservation::create([
            'reserv_date' => Now(),
            'issue_date' =>$request->input('issue_date'),
            'due_date' => $request->input('due_date'),
            'book_item_id' => $request->input('book_item_id'),
            'status_id' => Status::select('id')->where("slug",'pending')->first()->id,
            'user_id' => Auth::id(),
 
         ]);
            BookItem::find($request->input('book_item_id'))->update([
                'status_id' => Status::select('id')->where("slug",'reserved')->first()->id,
            ]);

         return Redirect::route('work_details', ['book', $request->input('book_id')]);
     
    }

    public function save_view(Request $datas){
        $work_id = $datas['work_id'];
        $model_name = $datas['model_name'];

        $class_name = "";
        if ( $model_name == "Report") {
           $class_name = Report::class;
        }else if ( $model_name == "Book") {
            $class_name = Book::class;
        }else if ( $model_name == "Subject") {
            $class_name = Subject::class;
         }

        View::create([
            'view_id' => $work_id,
            'view_type' => $class_name,
            'user_id' => Auth::id(),
         ]);

        return true;
    }

    public function like($model, $work_id, $isLiked){
        if($isLiked == 'true'){
            if($model == "Book"){
                Like::where(['like_type' => Book::class, 'like_id' => $work_id, 'user_id' => Auth::user()->id])->delete();
            
            }else if($model == "Report"){
                Like::where(['like_type' => Report::class, 'like_id' => $work_id, 'user_id' => Auth::user()->id])->delete();
            
            }else if($model == "Subject"){
                Like::where(['like_type' => Subject::class, 'like_id' => $work_id, 'user_id' => Auth::user()->id])->delete();
            }
        }else{
            if($model == "Book"){
                Like::create([
                    'like_type' => Book::class,
                    'like_id' => $work_id,
                    'user_id' => Auth::user()->id
                ]);  
            }else  if($model == "Report"){
                Like::create([
                    'like_type' => Report::class,
                    'like_id' => $work_id,
                    'user_id' => Auth::user()->id
                ]);  
            }else  if($model == "Subject"){
                Like::create([
                    'like_type' => Subject::class,
                    'like_id' => $work_id,
                    'user_id' => Auth::user()->id
                ]);
           }
          
        }

        if($model == 'Report'){
            $work = Report::where('id',$work_id)->with('authors')->with('school')->withCount('likes')->withCount('views')->with('field')->with('level')->first();
        }elseif($model == 'Book'){
            $work = Book::where('id',$work_id)->with('authors')->withCount('likes')->withCount('views')->with('publisher')->with('format')->with('type')->first();
            
        }elseif($model == 'Subject'){
            $work = Subject::where('id',$work_id)->with('author')->with('school')->withCount('likes')->withCount('views')->with('field')->with('period')->with('level')->first();
        }

        return response()->json($work);
    }

    public function rating($model, $work_id, $rating){
        $rate = null;
        if($model == 'Book'){
            $rate = Mark::where([
                'mark_type' => Book::class ,
                'mark_id' => $work_id,
                'user_id' => Auth::user()->id
            ])->first();
        }else  if($model == 'Report'){
            $rate = Mark::where([
                'mark_type' => Report::class ,
                'mark_id' => $work_id,
                'user_id' => Auth::user()->id
            ])->first();
        }else  if($model == 'Subject'){
            $rate = Mark::where([
                'mark_type' => Subject::class ,
                'mark_id' => $work_id,
                'user_id' => Auth::user()->id
            ])->first();
        }

        if(empty($rate)){
            if($model == 'Book'){
                Mark::create([
                    'mark_type' => Book::class,
                    'mark_id' => $work_id,
                    'user_id' => Auth::user()->id,
                    'value' => $rating
                ]);
            }else  if($model == 'Report'){
                Mark::create([
                    'mark_type' => Report::class,
                    'mark_id' => $work_id,
                    'user_id' => Auth::user()->id,
                    'value' => $rating
                ]);
            }else  if($model == 'Subject'){
                Mark::create([
                    'mark_type' => Subject::class,
                    'mark_id' => $work_id,
                    'user_id' => Auth::user()->id,
                    'value' => $rating
                ]);
            }
    
        }else{
            $rate->update([
                'value' => $rating
            ]);
        }
           
        $work = null;
        if($model == 'Report'){
            $reports = Report::where('id',$work_id)->with('authors')->with('school')->withCount('likes')->withCount('views')->with('field')->with('level')->first();
            $work = $reports;
        }elseif($model == 'Book'){
            $books = Book::where('id',$work_id)->with('authors')->withCount('likes')->withCount('views')->with('publisher')->with('format')->with('type')->first();
            $work = $books;
            
        }elseif($model == 'Subject'){
            $subjects = Subject::where('id',$work_id)->with('author')->with('school')->withCount('likes')->withCount('views')->with('field')->with('period')->with('level')->first();
            $work = $subjects;
        }


        return response()->json($work);
    }
}
