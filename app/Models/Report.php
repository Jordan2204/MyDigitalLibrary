<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $table = 'reports';
    protected $guarded = ['created_at'];
    
    //Make it available in the json response
    protected $appends = [
        'average_rating',
        'isLiked',
        'model_name'
    ];

    //implement the attribute
    public function getModelNameAttribute()
    {
        return 'Report';
    }
    public function authors(){
        return $this->morphToMany(Author::class,'author_work');
    }

    public function views(){
    return $this->morphMany(View::class,'view');
    }

    public function level(){
        return $this->belongsTo(Level::class, 'level_id');
    }

    public function school(){
        return $this->belongsTo(School::class, 'school_id');
    }

    public function field(){
        return $this->belongsTo(Field::class);
    }

    public function likes(){
        return $this->morphMany(Like::class, "like");
    }

    public function isLiked()
    {
        if (!auth()->check()) {
            return false;
        }
     
       return auth()->user()->likes->contains(function ($value, $key) {
            return $value->like_type == Report::class &&  $value->like_id == $this->id;
        });
    }

    public function marks(){
        return $this->morphMany(Mark::class, "mark");
    }
    
    function getAverageRatingAttribute(){
        return round($this->marks()->avg('value'),1);
    }

    function getIsLikedAttribute(){
        return $this->isLiked();
    }


}
