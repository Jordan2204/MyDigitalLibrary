<?php

namespace App\Http\Livewire;

use App\Models\Library;
use App\Models\WorkRight;
use Livewire\Component;

class Settings extends Component
{
    public $default_issue_days;
    public $max_issue_book_limit;
    public $max_reserv_book_limit;
    public $book_due_reminder_before_Days;
    public $fine_per_overdue_day;
    public $currency;
    public $lib_default_language;
    public $checked = ['unchecked', 'checked'];

    public $can_download_Book;
    public $can_download_Report;
    public $can_download_Subject;

    public $can_read_Book;
    public $can_read_Report;
    public $can_read_Subject;

    public $can_read;

    public function submit(){
        $this->validate([
            'lib_default_language' => ['required', 'string', 'max:255'],
            'book_due_reminder_before_Days' => ['required'],
            'max_reserv_book_limit' => ['required'],
            'default_issue_days' => ['required']
        ]);

        Library::find(1)->update([
            'lib_default_language' => $this->lib_default_language,
            'book_due_reminder_before_Days' => $this->book_due_reminder_before_Days,
            'max_reserv_book_limit' => $this->max_reserv_book_limit,
            'default_issue_days' => $this->default_issue_days,   
            'fine_per_overdue_day' => $this->fine_per_overdue_day,
            'max_issue_book_limit' => $this->max_issue_book_limit,
            'currency' => $this->currency
        ]);
 

        $this->alert('success', 'Settings  updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

    
    public function updateRightDownload($data){
   
        if($data[1] == "Book"){
            WorkRight::find($data[0])->update([
                'can_download' => $this->can_download_Book,
            ]);
        }

        if($data[1] == "Report"){
            WorkRight::find($data[0])->update([
                'can_download' => $this->can_download_Report,
            ]);
        }

        if($data[1] == "Subject"){
            WorkRight::find($data[0])->update([
                'can_download' => $this->can_download_Subject,
            ]);
        }

     }

    public function updateRightRead($data){
        if($data[1] == "Book"){
            WorkRight::find($data[0])->update([
                'can_read' => $this->can_read_Book,
            ]);
        }

        if($data[1] == "Report"){
            WorkRight::find($data[0])->update([
                'can_read' => $this->can_read_Report,
            ]);
        }

        if($data[1] == "Subject"){
            WorkRight::find($data[0])->update([
                'can_read' => $this->can_read_Subject,
            ]);
        }
    }

    public function render()
    {
        $work_rights = WorkRight::all();

        $book_right = WorkRight::where('work_type', 'Book')->first();
        $this->can_download_Book = $book_right->can_download;
        $this->can_read_Book = $book_right->can_read;

        $report_right = WorkRight::where('work_type', 'Report')->first();
        $this->can_download_Report = $report_right->can_download;
        $this->can_read_Report = $report_right->can_read;

        $subject_right = WorkRight::where('work_type', 'Subject')->first();
        $this->can_download_Subject = $subject_right->can_download;
        $this->can_read_Subject = $subject_right->can_read;



        return view('livewire.settings', 
        [
            'work_rights' => $work_rights, 
        ]);
    }
}
