<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
   public function index(){

    $auth = session('auth');
    $roles = session('roles');
    $isSuperAdmin = session('isSuperAdmin');
    $isAdmin = session('isAdmin');
    $isLibrarian = session('isLibrarian');
    $isMember = session('isMember');
    $isStudent = session('isStudent');
    $isLecturer = session('isLecturer');

    $session = [
            'auth' => $auth, 
            'roles' => $roles,
            'isSuperAdmin' => $isSuperAdmin,
            'isAdmin' => $isAdmin,
            'isLibrarian' => $isLibrarian,
            'isMember' => $isMember,
            'isLecturer' => $isLecturer,
            'isStudent' => $isStudent];

    return $session;
   }
}
