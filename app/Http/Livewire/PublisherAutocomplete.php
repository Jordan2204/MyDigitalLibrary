<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Publisher;

class PublisherAutocomplete extends Autocomplete
{
    protected $listeners = ['valueSelected', 'resetInputs', 'updatePublisherSearch'];


    public function valueSelected(Publisher $publisher)
    {
        $this->emitUp('publisherSelected', $publisher);
    }

    public function resetInputs(){
        $this->search = "";
    }

    public function updatePublisherSearch(Publisher $publisher){
        $this->search = $publisher->name;
    }


    public function query() {
        return Publisher::where('name', 'like', '%'.$this->search.'%')->orderBy('name');
    }   
}
