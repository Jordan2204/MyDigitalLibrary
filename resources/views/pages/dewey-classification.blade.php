@extends('../layouts/' . $layout)

@section('subhead')
    <title>Dewey Decimal Classification Management</title>
@endsection

@section('subcontent')
   @livewire('dewey-classification')
@endsection