<div>
    <div class="relative z-0 intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ __('Book Management') }}</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <x-jet-button wire:click="showCreateModal" class=" bg-theme-1 shadow-md mr-2 text-center">{{ __('New Book') }}</x-jet-button>    
            <div class="dropdown relative ml-auto sm:ml-0">
                <button class="dropdown-toggle button px-2 box text-gray-700">
                    <span class="w-5 h-5 flex items-center justify-center">
                        <i class="w-4 h-4" data-feather="plus"></i>
                    </span>
                </button>
                <div class="dropdown-box mt-10 absolute w-40 top-0 right-0">
                    <div class="dropdown-box__content box p-2">
                        <a href="" class="flex items-center p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                           {{ __('New Author') }}
                        </a>
                        <a href="" class="flex items-center p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">
                           {{ __('New Publisher') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: Datatable -->
    <div class="intro-y datatable-wrapper box p-2 pt-2 mt-5">
        @livewire('data-table.books-table')
    </div>
    <!-- END: Datatable -->
    <!-- Modal: Add Books -->
    <x-jet-dialog-modal :maxWidth="'6xl'" class="z-40" wire:model="showModalForm">
        @if($book_id)
        <x-slot name="ico">
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-2 ml-2 h-6 w-6 text-yellow-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
            </svg>
        </x-slot>
        <x-slot name="title">{{ __('Updating a Book') }}</x-slot>   
        @else
            <x-slot name="ico">
                <svg xmlns="http://www.w3.org/2000/svg" class="mt-2 ml-2 h-6 w-6 text-green-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </x-slot>
            <x-slot name="title">{{ __('Creation of a Book') }}</x-slot>  
        @endif
        <x-slot name="close">
            <a x-on:click.prevent @click="@this.closeModal()" href=""> 
                <svg xmlns="http://www.w3.org/2000/svg" class="mt-1 ml-2 text-white h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </a> 
        </x-slot>
     <x-slot name="content">
       <div class="space-y-4 divide-y divide-gray-200">
                <form enctype="multipart/form-data"> 
                @csrf
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 text sm:pb-4"> 
                    <div class="grid grid-cols-1 sm:grid-cols-3 gap-4 mb-3">
                        <div class="grid grid-cols-1 sm:grid-cols-2 col-span-2 gap-4">
                            <div class="col-span-2 sm:col-span-1"> 
                                <x-jet-label for="type" value="{{ __('Type Of Book') }}" />
                                <select id="type" wire:key="type" wire:model="type" name="type" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                    <option selected value="null"> {{ __('select a type') }}</option>
                                    @foreach ($types as $type)
                                        <option value="{{ $type->id }}">{{ $type->title }}</option>
                                    @endforeach
                                </select>   
                                 @error('type') <span class="text-red-500">{{ $message }}</span>@enderror 
                            </div> 

                            <div wire:key="ISBN" class="col-span-2 sm:col-span-1"> 
                                <x-jet-label for="title" value="{{ __('ISBN/ISSN') }}" />
                                <input id="ISBN" name="ISBN" wire:model="ISBN" type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2" placeholder="isbn" required autofocus autocomplete="isbn">
                                @error('ISBN') <span class="text-red-500">{{ $message }}</span>@enderror 
                            </div> 
                           
                            <div wire:key="title" class="col-span-2 sm:col-span-1"> 
                                <x-jet-label for="title" value="{{ __('Title') }}" />
                                <input id="title" name="title" wire:model="title" type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2" placeholder="title" required autofocus autocomplete="title">
                                <div>
                                    @error('title') <span class="text-red-500">{{ $message }}</span>@enderror 
                                </div>
                            </div> 

                            <div wire:key="number_of_pages" class="col-span-2 sm:col-span-1"> 
                                <x-jet-label for="number_of_pages" value="{{ __('Number of Pages') }}" />
                                <input id="number_of_pages" name="number_of_pages" wire:model="number_of_pages" type="number" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2" placeholder="number of pages" required autofocus autocomplete="number_of_pages">
                                <div>
                                    @error('number_of_pages') <span class="text-red-500">{{ $message }}</span>@enderror 
                                </div>
                            </div>

                            <div wire:key="language" class="col-span-2 sm:col-span-1"> 
                                <x-jet-label for="language" value="{{ __('Language') }}" />
                                <select id="language" wire:key="language" wire:model="language" name="language" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                    <option selected value="null"> {{ __('select a language') }}</option>
                                    <option value="FR">{{ __('French') }}</option>
                                    <option value="EN">{{ __('English') }}</option>
                                </select>   
                                <div>
                                    @error('language') <span class="text-red-500">{{ $message }}</span>@enderror 
                                </div>
                            </div>

                            <div wire:key="publisher" class="col-span-2 sm:col-span-1"> 
                                <div class="flex justify-between">
                                    <x-jet-label for="publisher" value="{{ __('Publisher') }}" />
                                    @if($createPublisher)
                                        <a x-on:click.prevent href="" wire:click="createPublisher" class="flex items-center text-theme-4 mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                        </svg> {{ __('create') }}
                                        </a> 
                                    @endif   
                                </div>

                                <div>
                                 @livewire('publisher-autocomplete',['var_name' => 'publisher', 'model_name' => 'publisher'])
                                </div>   
                                <div>@error('publisher') <span class="text-red-500">{{ $message }}</span>@enderror 
                         
                                </div>
                            </div>

                            <div wire:key="ddc" class="col-span-2 sm:col-span-2"> 
                                <x-jet-label for="title" value="{{ __('Dewey Code') }}" />
                                <div class="flex ">
                                    <div class="mr-2 w-1/3">
                                        <select id="cat1_val" wire:key="cat1_val" wire:model="cat1_val" name="cat1_val" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                            <option selected value=""> {{ __('select a class') }}</option>
                                            @foreach ($cat1 as $cat)
                                                <option value="{{ $cat->integer_code }}">{{ $cat->integer_code }} : {{ $cat->integer_title }} </option>
                                            @endforeach
                                        </select>   
                                        @error('cat1_val') <span class="text-red-500">{{ $message }}</span>@enderror 
                                    </div>
                                    <div class="mr-2 w-1/3">
                                        <select id="cat2_val" wire:key="cat2_val" wire:model="cat2_val" name="cat2_val" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                            <option selected value=""> {{ __('select a division') }}</option>
                                            @foreach ($cat2 as $cat)
                                                <option value="{{ $cat->integer_code }}">{{ $cat->integer_code }} : {{ $cat->integer_title }} </option>
                                            @endforeach
                                        </select>   
                                        @error('cat2_val') <span class="text-red-500">{{ $message }}</span>@enderror 
                                    </div>
                                    
                                    <div class="w-1/3">
                                        <select id="cat3_val" wire:key="cat3_val" wire:model="cat3_val" name="cat3_val" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                            <option selected value=""> {{ __('select a section') }}</option>
                                            @foreach ($cat3 as $cat)
                                                <option value="{{ $cat->integer_code }}">{{ $cat->integer_code }} : {{ $cat->integer_title }} </option>
                                            @endforeach
                                        </select>   
                                        @error('cat3_val') <span class="text-red-500">{{ $message }}</span>@enderror 
                                    </div>
                                   
                                </div>
                                @error('ISBN') <span class="text-red-500">{{ $message }}</span>@enderror 
                            </div> 
                            
                            <div wire:key="author_1" class="col-span-2 sm:col-span-1"> 
                                <div class="flex justify-between">
                                <x-jet-label for="author_1" value="{{ __('Principal Author') }}" />
                                    @if($createAuthor)
                                        <a x-on:click.prevent href="" wire:click="createAuthor" class="flex items-center text-theme-4 mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                        </svg> {{ __('create') }}
                                        </a> 
                                    @endif   
                                </div>
                                <div>
                                    @livewire('author-autocomplete',['var_name' => 'author_1', 'model_name' => 'author'])
                               </div>
                               <div>@error('author_1') <span class="text-red-500">{{ $message }}</span>@enderror </div>
                            </div>

                            <div wire:key="title" class="col-span-2 sm:col-span-2"> 
                                <x-jet-label for="title" value="{{ __('Description') }}" />
                                <textarea  id="description" wire:model="description" data-feature="basic" class="w-full border" name="description"></textarea>
                                <div>
                                    @error('description') <span class="text-red-500">{{ $message }}</span>@enderror 
                                </div>
                            </div> 

                            
                        </div> 
    
                        <div class="grid grid-cols-1 sm:grid-cols-2 gap-4">
                            <div class="col-span-2 border border-gray-200 rounded-md p-5">
                                <div class="w-full h-80 relative image-fit cursor-pointer zoom-in mx-auto">
                                    @if ($cover && !is_string($cover))
                                    <img class="rounded-md" alt="Cover Manual" src="{{ $cover->temporaryUrl() }}">
                                    @elseif($cover && is_string($cover))
                                    <img class="rounded-md" alt="Cover Update" src="/storage/{{ $this->cover }}">
                                     @else
                                    <img class="rounded-md" alt="Cover Default" src="{{ asset('/storage/images/awaiting_cover.jpg') }}">
                                    @endif
                                    <div class="tooltip w-5 h-5 flex items-center justify-center absolute rounded-full text-white bg-theme-6 right-0 top-0 -mr-2 -mt-2 tooltipstered"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x w-4 h-4"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> </div>
                                </div>
                                <div class="w-40 mx-auto cursor-pointer relative mt-5">
                                    <button type="button" class="button w-full bg-theme-1 text-white">{{ __('Change Cover') }}</button>
                                    <input type="file" id="cover" wire:model="cover" name="cover" class="w-full h-full top-0 left-0 absolute opacity-0">
                                    @error('cover') <span class="error text-red-600">{{ $message }}</span> @enderror
        
                                </div>
                            </div>
                        </div>    
                    </div>
                </div> 
            </form> 
        </div>
     </x-slot>
    
     <x-slot name="footer">
       @if($book_id)
       <x-jet-button wire:loading.attr="disabled" wire:click="updateBook">  {{ __('Update') }}</x-jet-button>
       @else
       <x-jet-button wire:loading.attr="disabled" wire:click="storeBook"> {{ __('Create') }}</x-jet-button>
       @endif
       <div wire:loading wire:target="updateBook" class="mt-1">
        <svg width="" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" fill="rgb(45, 55, 72)" class="content-center w-6 h-6">
            <circle cx="15" cy="15" r="15">
                <animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate>
                <animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate>
            </circle>
            <circle cx="60" cy="15" r="9" fill-opacity="0.3">
                <animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite"></animate>
                <animate attributeName="fill-opacity" from="0.5" to="0.5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite"></animate>
            </circle>
            <circle cx="105" cy="15" r="15">
                <animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate>
                <animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate>
            </circle>
        </svg>
    </div>
    <div wire:loading wire:target="storeBook" class="mt-1">
        <svg width="" viewBox="0 0 120 30" xmlns="http://www.w3.org/2000/svg" fill="rgb(45, 55, 72)" class="content-center w-6 h-6">
            <circle cx="15" cy="15" r="15">
                <animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate>
                <animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate>
            </circle>
            <circle cx="60" cy="15" r="9" fill-opacity="0.3">
                <animate attributeName="r" from="9" to="9" begin="0s" dur="0.8s" values="9;15;9" calcMode="linear" repeatCount="indefinite"></animate>
                <animate attributeName="fill-opacity" from="0.5" to="0.5" begin="0s" dur="0.8s" values=".5;1;.5" calcMode="linear" repeatCount="indefinite"></animate>
            </circle>
            <circle cx="105" cy="15" r="15">
                <animate attributeName="r" from="15" to="15" begin="0s" dur="0.8s" values="15;9;15" calcMode="linear" repeatCount="indefinite"></animate>
                <animate attributeName="fill-opacity" from="1" to="1" begin="0s" dur="0.8s" values="1;.5;1" calcMode="linear" repeatCount="indefinite"></animate>
            </circle>
        </svg>
    </div>
     </x-slot>
    </x-jet-dialog-modal>
    
    <x-jet-confirmation-modal wire:model="showDeleteModalForm">
        <x-slot name="title">{{ __('Deletion of a Book') }}</x-slot>
        <x-slot name="close">
            <a x-on:click.prevent @click="@this.closeModal()" href=""> 
                <svg xmlns="http://www.w3.org/2000/svg" class="mt-1 ml-2 text-white h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </a> 
        </x-slot>
        <x-slot name="content">
            <div class="space-y-8 text-2xl divide-y divide-gray-200">
                <div class="p-5 text-center"> <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                    @if($this->book_id)
                    <p>{{ __('Do you really want to delete this') }} <span class="text-gray-900 text-bold">{{ __('Data')}} ?</span>{{ __('This process cannot be undone')}}.</p>
                    @endif
                </div>      
            </div>
        </x-slot>
        <x-slot name="footer">
           <x-jet-button wire:loading.attr="disabled" wire:click="deleteBook({{ $this->book_id }})" class="bg-red-700">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
            </svg>
             {{ __('Delete') }}
        </x-jet-button>
        </x-slot>
    </x-jet-confirmation-modal>
</div>


