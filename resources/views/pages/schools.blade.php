@extends('../layouts/' . $layout)

@section('subhead')
    <title>{{ __('School Management') }}</title>
@endsection

@section('subcontent')
    @livewire('schools')
@endsection