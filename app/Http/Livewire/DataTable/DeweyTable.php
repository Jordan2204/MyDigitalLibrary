<?php

namespace App\Http\Livewire\DataTable;

use App\Models\DDCInteger;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class DeweyTable extends LivewireDatatable
{
    public $model = DDCInteger::class;

    public $hideable = 'select';
    public $exportable = true;
    public $integer_title;
    public $id_to_update;
    
    protected $listeners =
    [
        'newQuery' => 'refresh_data',
        'updateQuery' => 'refresh_data',
        'deleteQuery' => 'refresh_data'
    ];

    public function builder()
    {
        return  DDCInteger::query();
    }

    public function showEditInput($id){
       $this->id_to_update = $id;
       $this->integer_title = DDCInteger::find($id)->integer_title;
   }

    public function showUpdateInput($id){
        $this->emitUp('update_input', [$id, $this->integer_title]);
    }

    public function refresh_data()
    {
        $this->id_to_update = null;
        $this->emit('updateSavedQueries');    
    }

    public function columns()
    {
        return [

            Column::name('integer_code')
                ->label(__('Code'))
                ->searchable()
                ->alignCenter(),

            Column::callback(['id', 'integer_title'], function ($id, $integer_title) {
                return view('datatables.category-dewey', 
                ['id' => $id, 'slot'=> $id, 'integer_title' => $integer_title, 'type' => "DDC", 'id_to_update' => $this->id_to_update]);
            })->label('Categorie')
            ->searchable(),
         
            Column::callback(['id'], function ($id) {
                return view('datatables.table-actions', 
                ['id' => $id, 'type' => "DDC", 'id_to_update' => $this->id_to_update]);
            })->label('Actions')
            ->unsortable()
           
        ];
    }
}