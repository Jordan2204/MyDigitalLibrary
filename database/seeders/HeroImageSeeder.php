<?php

namespace Database\Seeders;

use App\Models\Library;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HeroImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 8; $i++) { 
            DB::table('medias')->insert([
                'media_id' => 1,
                'media_type' => Library::class,
                'url' => "images/hero".$i.'.jpg',
                'type' => "Hero Image",
                'slug' => "hero-image"
            ]);
        }
       

    }
}
