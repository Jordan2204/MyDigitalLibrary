@extends('../layouts/'. $layout)

@section('subhead')
    <title>Dashboard</title>
@endsection

@section('subcontent')
   @livewire('dashboard')
@endsection