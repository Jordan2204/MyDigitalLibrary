<?php

namespace App\Http\Livewire;

use App\Models\Field;
use App\Models\Level;
use App\Models\Author;
use App\Models\Report;
use App\Models\School;
use Livewire\Component;
use App\Models\AuthorWork;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class StudentReports extends Component
{
    use WithFileUploads;

    public $report_id;
    public $title;
    public $level;
    public $cover;
    public $url;
    public $new_url;
    public $field;
    public $school;
    public $author_1;
    public $author_2;
    public $academic_year;
    public $number_of_pages;
    public $data;

    public $showModalForm = false;
    public $showDeleteModalForm = false;
    public $createAuthor = False;
   
    protected $listeners = [
        'author_1' => 'author_1',
        'author_2' => 'author_2',
        'createModel' => 'createModel',
        'dontCreateModel' => 'dontCreateModel',
        'showEditModal' => 'showEditModal',
        'showDeleteModal' => 'showDeleteModal'
    ];

    public function showCreateModal()
    {
        $this->emit('resetInputs');
        $this->resetValidation();
        $this->reset();
        $this->showModalForm = true;
    }
    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }

    public function author_1(Author $author)
    {
        $this->author_1 = $author;
     
    }

    public function author_2(Author $author)
    {
        $this->author_2 = $author;
    }

    public function createModel($data){
        $this->data = $data;
        if ($data[0] == "author") {
            $this->createAuthor = True;
        }
    }

    public function dontCreateModel(){
        $this->createAuthor = False;
    }

    public function createAuthor(){
        Author::create(["name" => $this->data[1]]);
        $this->author_1 = $this->data[1];
        $this->alert('success', 'Author '.$this->data[1].' created', [
            'position' =>  'top-end', 
            'timer' =>  '9000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

    public function store()
    {
        $this->validate([
            'title' => ['required', 'string', 'max:255'],
            'field' => ['required'],
            'author_1' => ['required', 'max:255'],
            'url' => ['required','file', 'mimes:pdf','max:52288']
            ]);
        $cover_name = '';
        $url_name = '';
        if ($this->url) {
            $url_extension = $this->url->getClientOriginalExtension();
            $url_file_name = 'reports'.'/'.Str::random(40);
            $url_name = $url_file_name.'.'.$url_extension;
            $this->url->storeAs('public/', $url_name);
         

            if ($this->cover) {
                $cover_extension = $this->cover->getClientOriginalExtension();
                $cover_name = 'reports'.'/'.Str::random(40).'.'.$cover_extension;
                $this->cover->storeAs('public/',  $cover_name);
            }else{
                //Path
                $base_path = base_path();
                $base_final_path = str_replace("\\", "/", $base_path);
                $cover_name = 'reports'.'/'.Str::random(40).'.png';
        
                $Imagick = new \Imagick();
                $Imagick->readImage($base_final_path.'/public/storage'.'/'.$url_name.'[0]'); 
                $Imagick->setImageBackgroundColor('#ffffff');
                $Imagick->setImageAlphaChannel(\Imagick::ALPHACHANNEL_REMOVE);
                $Imagick = $Imagick->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN); 
                $Imagick->setImageFormat('png');
                $Imagick->writeImage($base_final_path.'/public/storage'.'/'.$cover_name);

            }
          
        }

        $report = Report::create([
            'title' => $this->title,
            'academic_year' => $this->academic_year,
            'cover' => $cover_name,
            'url' => $url_name,
            'level_id' => $this->level,
            'field_id' => $this->field,
            'school_id' => $this->school,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
        ]);
        
        if ($this->author_1) {
            AuthorWork::create([
                'author_work_id' => $report->id,
                'author_work_type' => Report::class,
                'author_id' => $this->author_1->id,
                'status'  => true,
             ]);
        }
        if ($this->author_2) {
            AuthorWork::create([
                'author_work_id' => $report->id,
                'author_work_type' => Report::class,
                'author_id' =>  $this->author_2->id,
                'status'  => false,
             ]);
        }
        
        $this->alert('success', 'The Report '.$this->title.' was created Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
        $this->reset();
        $this->emit('newQuery');
       
        }
    public function update()
    {
        $this->validate([
            'title' => ['required', 'string', 'max:255'],
            'author_1' => ['required', 'string', 'max:255'],
            ]);

        $cover_name = $this->cover;
        $url_name = $this->url;
      if (!is_string($this->url)) {
        Storage::delete('public/', $this->new_url);
        $this->new_url = 'reports'.'/'.Str::random(40).'.'.$this->url->getClientOriginalExtension();
        $this->url->storeAs('public/', $this->new_url);
    }

        Report::find($this->report_id)->update([
            'title' => $this->title,
            'academic_year' => $this->academic_year,
            'cover' => $cover_name,
            'url' => $url_name,
            'level_id' => $this->level,
            'field_id' => $this->field,
            'school_id' => $this->school,
            'updated_by' => Auth::user()->id,
      
        ]);
        $this->alert('success', 'The Report '.$this->title.' was updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
        $this->reset();
        $this->emit('updateQuery');
    }

    public function showEditModal($id)
    {
        $this->reset();
        $this->resetValidation();
        $this->showModalForm = true;
        $this->report_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $report = Report::findOrFail($this->report_id);
        $this->title =  $report->title;
        $this->number_of_pages =  $report->number_of_pages;
        if(!empty($report->authors[0])){
            $this->author_1 =  $report->authors[0]->name;
            $this->emit('updateAuthor1Search', $report->authors[0]->id);
        }
        if(!empty($report->authors[1])){
            $this->author_2 =  $report->authors[1]->name;
            $this->emit('updateAuthor2Search', $report->authors[1]->id);
        }
        $this->url = $report->url;
        $this->academic_year = $report->academic_year;
        $this->cover = $report->cover;
        $this->field = $report->field->id;
        $this->level = $report->level->id;
        $this->school = $report->school->id;
    }

    public function showDeleteModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->report_id = $id;
    }

    public function delete($id)
    {
        $report = Report::find($id)->with('authors')->first();
        Storage::delete('public/'.$report->url);
        Storage::delete('public/'.$report->cover);
        $report->delete();
        $this->reset();
        $this->showDeleteModalForm = false;
        $this->alert('success', 'The Report '.$report->title.' was deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);

        $this->emit('deleteQuery');
    }

    public function render()
    {
        $authors = Author::all();
        $levels = Level::all();
        $schools = School::all();
        $fields = Field::all();
        return view('livewire.student-reports', [
            'authors' => $authors,
            'levels' => $levels,
            'schools' => $schools,
            'fields' => $fields
            ]);
    }
}
