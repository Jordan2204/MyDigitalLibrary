<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DDCSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=-1; $i < 1000-1; $i++) { 
            $num_code = $i+1;
            $taille_code = strlen(strval($num_code));

            $new_code = '';
            for ($j=0; $j <3 - $taille_code ; $j++) { 
                $new_code.='0';
            }
            $new_code.= $num_code;

            if($new_code == "000") {
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'Computer science, information and general works',
                ]);
            }else if($new_code == "100"){
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'Philosophy and psychology',
                ]);

            }else if($new_code == "200"){
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'Religion',
                ]);

            }else if($new_code == "300"){
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'Social sciences',
                ]);

            }else if($new_code == "400"){
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'Language',
                ]);

            }else if($new_code == "500"){
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'Pure Science',
                ]);

            }else if($new_code == "600"){
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'Technology',
                ]);

            }else if($new_code == "700"){
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'Arts and recreation',
                ]);

            }else if($new_code == "800"){
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'Literature',
                ]);

            }else if($new_code == "900"){
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                    'integer_title' => 'History and geography',
                ]);
                
            }else{
                DB::table('ddc_integers')->insert([
                    'integer_code' => $new_code,
                ]);
            }
        

            // for ($j=-1; $j < 1000-1 ; $j++) { 
            //     $ddc_integers =  DB::table('ddc_integers')->where('integer_code', $new_code)->first();
            //     $num_code_d = $j+1;
            //     $taille_code_d = strlen(strval($num_code_d));
    
            //     $new_code_d = '';
            //     for ($k=0; $k <3 - $taille_code_d ; $k++) { 
            //         $new_code_d.='0';
            //     }
            //     $new_code_d.= $num_code_d;
            //     DB::table('ddc_decimals')->insert([
            //         'decimal_code' => $new_code,
            //         'ddc_integer_id'  => $ddc_integers->id,
            //     ]);
            // }
        }
    }
}
