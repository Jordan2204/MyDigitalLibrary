<?php

namespace App\Http\Livewire;

use App\Models\Media;
use App\Models\Library;
use Livewire\Component;
use App\Models\SocialMedia;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class HomeSettings extends Component
{
    use WithFileUploads;
 
    public $facebook_link;
    public $linkedIn_link;
    public $twitter_link;
    public $email_link;
    public $phone1;
    public $phone2;
    public $lib_name;
    public $lib_desc;
    public $hero_image_title;
    public $hero_image_desc;
    public $hero_images = [];
    public $old_logo;
    public $old_favicon;
    public $logo;
    public $favicon;
    protected $pageController;
    
    public function mount(){
        $library = Library::find(1);
        $this->lib_name =  $library->lib_name;
        $this->lib_desc = $library->lib_desc;
        $this->email_link = $library->lib_email;
        $this->phone1 = $library->phone1;
        $this->phone2 = $library->phone2;
        $this->hero_image_title = $library->hero_image_title;
        $this->hero_image_desc = $library->hero_image_desc;


        $this->facebook_link = SocialMedia::find(1)->url;
        $this->linkedIn_link = SocialMedia::find(2)->url;
        $this->twitter_link = SocialMedia::find(3)->url;

        $this->hero_images = Media::where("slug", "hero-image")->get()->pluck('url')->toArray();
    
    }
    public function updatedPhoto()
    {
        $this->validate([
            'hero_images' => 'image|max:1024',
        ]);
    }

    public function submit(){
        $this->validate([
            'email_link' => ['required', 'string', 'max:255'],
            'phone1' => ['required'],
         ]);

        Library::find(1)->update([
            'lib_name' => $this->lib_name,
            'lib_desc' => $this->lib_desc,
            'lib_email' => $this->email_link,
            'phone1' => $this->phone1,
            'phone2' => $this->phone2,
            'hero_image_title' => $this->hero_image_title,   
            'hero_image_desc' => $this->hero_image_desc,
        ]);

        //Hero Images
        foreach ($this->hero_images as $hero_image){
            if(!is_string($hero_image)){
                $url_image ="";
                $url_extension = $hero_image->getClientOriginalExtension();
                $image_name = 'images'.'/'.Str::random(40);
                $image_name = $image_name.'.'.$url_extension;
                $hero_image->storeAs('public/', $image_name);

                Media::create([
                    'media_id' => 1,
                    'media_type' => Library::class,
                    'url'  => $image_name,
                    'type'  => "Hero Image",
                    'slug'  => "hero-image"
                ]);
            }
        }

        SocialMedia::find(1)->update([
            'url' => $this->facebook_link,
        ]);
        
        SocialMedia::find(2)->update([
            'url' => $this->linkedIn_link,
        ]);

        SocialMedia::find(3)->update([
            'url' => $this->twitter_link,
        ]);
        
        $this->alert('success', 'Home Settings  updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

    public function delete_file($type, $position){
        
        if($type == "string"){
            Media::where("url", $this->hero_images[$position])->delete();
            Storage::delete('public/', $this->hero_images[$position]);
        }

        unset($this->hero_images[$position]);
    }
  
    public function render()
    {
    
        return view('livewire.home-settings');
    }
}
