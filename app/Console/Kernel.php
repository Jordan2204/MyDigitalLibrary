<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use App\Mail\ReservationCancelMail;
use Illuminate\Support\Facades\Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        //verify Overdue delay of bookings
        $schedule->call(function () {
            $status_cancel = DB::table('status')->where('slug', 'cancelled')->first();
            $status_available = DB::table('status')->where('slug', 'available')->first();
          
            $bookings = DB::table('book_reservations')->where('status_id', '!=', $status_cancel->id)->get();
            $now = date("Y-m-d H:i:s");
            foreach ($bookings as $booking){
                if($now > $booking->issue_date){
                    //can cancel
                    $user = DB::table('users')->where('id', $booking->user_id)->first();
                    
                    DB::beginTransaction();
                    try {
                        DB::update(
                            'update book_reservations set status_id = ? where id = ?',
                            [$status_cancel->id, $booking->id]);
    
                        DB::update(
                                'update book_items set status_id = ? where id = ?',
                                [$status_available->id, $booking->book_item_id]);
    
                        DB::commit();
    
                        $reservation_detail = [
                            "issue_date" =>  $booking->issue_date,
                            "reservation_id"  =>  $booking->id,
                         ];
                       Mail::to($user->email)->send(new ReservationCancelMail($reservation_detail));
    
                    } catch (\Throwable $th) {
                        DB::rollBack();
                    }
                }  
            }
    
        })->twiceDaily(10, 20);

        

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
