@if (app()->isLocal()) @endif
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!-- BEGIN: Head -->
<head>
    <meta charset="utf-8">
    <link href="{{ asset('dist/images/logo.png') }}" rel="shortcut icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="My digital Library">
    <meta name="keywords" content="Digital Library template, e-book, books, Lybrary management system, web app">
    <meta name="author" content="ANAFACK Diderot Jordan">

    <!-- BEGIN: CSS Assets-->
    <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}" />
    <!-- END: CSS Assets-->
    @livewireStyles
    <script src="{{ asset('dist/js/alpine.js') }}" defer></script>    

    @yield('head')

</head>
<!-- END: Head -->
    @yield('body')
    
</html>