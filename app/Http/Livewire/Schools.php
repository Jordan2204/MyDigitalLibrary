<?php

namespace App\Http\Livewire;

use App\Models\School;
use Livewire\Component;

class Schools extends Component
{
    public $school_id;
    public $title;
    public $abbr;
    public $description;
    public $showModalForm = false;
    public $showDeleteModalForm = false;

    protected $listeners = [
        'showEditModal' => 'showEditModal',
        'showDeleteModal' => 'showDeleteModal'
    ];


    public function showCreateModal()
    {
        $this->resetValidation();
        $this->showModalForm = true;
    }
    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }
    
    public function store()
    {
        $this->validate([
            'title' =>'required',
            'abbr' =>'required'
        ]);

        $school = School::create([
            'title' => $this->title,
            'abbr' => $this->abbr,
            'description' => $this->description
        ]);
        $this->reset();
        $this->emit('newQuery');
        //session()->flash('flash.banner', 'Permission created Successfully');
    }
    public function update()
    {
        $this->validate([
            'title' =>'required',
            'abbr' =>'required'
      ]);

        School::find($this->school_id)->update([
            'title' => $this->title,
            'abbr' => $this->abbr,
            'description' => $this->description
        ]);
        $this->reset();
        $this->emit('updateQuery');
        //session()->flash('flash.banner', 'Post Updated Successfully');
    }

    public function showEditModal($id)
    {
        $this->reset();
        $this->showModalForm = true;
        $this->school_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $school = School::findOrFail($this->school_id);
        $this->title = $school->title;
        $this->abbr = $school->abbr;
        $this->description = $school->description;
    }

    public function showDeleteModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->school_id = $id;
    }

    public function delete($id)
    {
        $school = School::find($id);
        $school->delete();
        $this->showDeleteModalForm = false;
        $this->emit('deleteQuery');
    }

    public function render()
    {
        return view('livewire.schools');
    }
}
