@extends('../layouts/' . $layout)

@section('subhead')
    <title>{{__("Users Management")}}</title>
@endsection

@section('subcontent')
    @livewire('admins')
@endsection