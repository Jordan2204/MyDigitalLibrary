<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Inertia\Inertia;
use App\Models\Media;
use App\Models\Report;
use App\Models\Library;
use App\Models\Subject;
use App\Models\Testimonial;
use Mockery\Matcher\Subset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\RegistrationCodeModel;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use App\Http\Livewire\RegistrationCode;
use phpDocumentor\Reflection\Types\This;
use App\Http\Controllers\SessionController;
use Laravel\Fortify\Http\Controllers\AuthenticatedSessionController;

class HomeController extends Controller
{
    protected $sessionController;
    public function __construct(SessionController $sessionController){
        $this->sessionController = $sessionController;
    }

    public function work_count(){
        $reports_count = Report::count();
        $books_count = Book::count();
        $subjects_count = Subject::count();

        return ["reports_count" => $reports_count, "books_count" => $books_count, "subjects_count" => $subjects_count];
    }

    public function testimonial(Request $request){
        Testimonial::create([
            'title' => $request->occupation,
            'username' => $request->name,
            'description' => $request->content,
            'profile_photo_path' => Auth::user()->profile_photo_path ?? "/dist/images/user_default.png"
        ]);
           
        $testimonials = Testimonial::all();


        return response()->json($testimonials);
    }

    public function home(){
        $books = Book::with('authors')->withCount('views')->withCount('likes')->with('publisher')->with('format')->with('type')->limit(10)->orderBy('id', 'DESC')->get();
        $reports = Report::with('authors')->withCount('views')->withCount('likes')->with('field')->with('level')->limit(10)->orderBy('id', 'DESC')->get();
        $subjects = Subject::with('author')->withCount('views')->withCount('likes')->with('field')->with('period')->with('level')->limit(10)->orderBy('id', 'DESC')->get();
        $work_count = $this->work_count();
        $hero_images = Media::where("slug", "hero-image")->get()->pluck('url')->toArray();

        $testimonials = Testimonial::all();

        $works = new \Illuminate\Database\Eloquent\Collection; 
        $works = $works->concat($books);
        $works = $works->concat($reports);
        $works = $works->concat($subjects);
        $works = $works->shuffle();
        return Inertia::render('Home/index', [
            'auth' => Auth::check(),
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
            'works' =>  $works,
            'testimonials' =>  $testimonials,
            'work_count' =>  $work_count,
            'hero_images' =>  $hero_images,
            'session' => $this->sessionController->index()
        ]);
    }

    public function about(){
        return Inertia::render('About/index', [
            'auth' => Auth::check(),
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
            'session' => $this->sessionController->index()
   
        ]);
    }

    public function check_reg_code(Request $request){
        $reg_code = RegistrationCodeModel::where('code', $request->registration_code)->with('status')->first();
        if(!empty($reg_code)){
            if($reg_code->status->slug == "available"){
                return response()->json(0); 
            }else{
                return response()->json(1); 
            }
        }else{
            return response()->json(2); 
        }
       
    }

    public function help(){
        $work_count = $this->work_count();

        return Inertia::render('Help/index', [
            'auth' => Auth::check(),
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
            'session' => $this->sessionController->index(),
            'work_count' => $work_count

        ]);
    }

    public function physicalLibrary(){
        //Select a book  if the format is physical
        $works = Book::whereHas('bookItems.format', function ($query) {
            return $query->where('slug', '=', "p_doc");
        })->with('bookItems')->with('authors')->with('publisher')->get();

        return Inertia::render('PhysicalLibrary/index', [
            'auth' => Auth::check(),
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
            'works' => $works,
            'session' => $this->sessionController->index()
        ]);
    }

    public function work_details($type,$id){
        $work = null;
        $related = null;
       if ($type == "report") {
            $work = Report::where('id', $id)->with('authors')->withCount('likes')->with('school')->with('field')->with('level')->first();
            $related = Report::where('title','like','%'.$work->title.'%')->with('authors')->with('field')->with('level')->get();
      
        }elseif ($type == "book") {
            $work = Book::where('id', $id)->with('authors')->withCount('likes')->with('bookItems',function($query){
                $query->with('format')->with('status');
            })->with('publisher')->first();
            $related = Book::where('title','like','%'.$work->title.'%')->with('authors')->with('publisher')->get();
            //dd($work);
        }elseif ($type == "subject") {
            $work = Subject::where('id', $id)->with('author')->withCount('likes')->with('school')->with('field')->with('level')->with('period')->first();
            $related = Subject::where('title','like','%'.$work->title.'%')->with('author')->with('field')->with('level')->get();
        }
    
        $related = $related->slice(0, 4);
     
        return Inertia::render('WorkDetails/index', [
            'auth' => Auth::check(),
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
            'work' =>  $work,
            'related' =>  $related,
            'session' => $this->sessionController->index()
        ]);
    }
}

