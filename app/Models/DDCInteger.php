<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DDCInteger extends Model
{
    use HasFactory;

    protected $guarded = ['created_at'];
    protected $table = 'ddc_integers';
   
    
    public function decimal_code(){
        $this->hasMany(DDCInteger::class, 'ddc_integer_id');
    }
}
