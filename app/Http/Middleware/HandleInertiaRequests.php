<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Inertia\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Models\Library;
use App\Models\SocialMedia;
use App\Models\WorkRight;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that's loaded on the first page visit.
     *
     * @see https://inertiajs.com/server-side-setup#root-template
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determines the current asset version.
     *
     * @see https://inertiajs.com/asset-versioning
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Defines the props that are shared by default.
     *
     * @see https://inertiajs.com/shared-data
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        return array_merge(parent::share($request), [
            'auth' => function () {
                return Auth::check();
            },
            'isAdmin' => function () {
                return session('isAdmin');
            },
            'isLibrarian' => function () {
                return session('isLibrarian');
            },
            'locale' => function () {
                return app()->getLocale();
            },
            
            'language' => function () {  
                return translations(
                    resource_path('lang/'. app()->getLocale() .'.json')
                );
            },
            'library' => function () {  
                return Library::first();
            },
            'social_medias' => function () {  
                return SocialMedia::all();
            },
            'can_download_book' => function () {  
                return WorkRight::where('work_type', 'Book')->first()->can_download;
            },
            'can_download_report' => function () {  
                return WorkRight::where('work_type', 'Report')->first()->can_download;
            },
            'can_download_subject' => function () {  
                return WorkRight::where('work_type', 'Subject')->first()->can_download;
            }
        ]);
    }
}
