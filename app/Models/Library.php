<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Library extends Model
{
    use HasFactory;

    protected $table = "libraries";
    protected $guarded = ['id','created_at'];
  
    public function images(){
        return $this->MorphMany(Media::class, 'mediable','media_type','media_id');
    }

    public function hero_images(){
        return $this->images->where('slug', 'hero-image');
    }
}
