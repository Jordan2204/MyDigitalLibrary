@extends('../layouts/base')

@section('body')
    <body class="app">
        @yield('content')

        <!-- BEGIN: JS Assets
        <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=['your-google-map-api']&libraries=places"></script> -->
        <script src="{{ asset('dist/js/app.js') }}" defer></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        @yield('scripts')
        @yield('before-livewire-scripts')
        @livewireScripts
        @yield('after-livewire-scripts')
        @yield('alpine-plugins')

        <!-- Alpine Core -->
        <script src="{{ asset('dist/js/alpine.js') }}" defer></script>   
        @yield('modals')
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <x-livewire-alert::scripts />
        <!-- END: JS Assets-->
    </body>
@endsection 