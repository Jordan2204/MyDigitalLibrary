@extends('../layouts/' . $layout)

@section('subhead')
    <title>{{ __('Field Management') }}</title>
@endsection

@section('subcontent')
    @livewire('fields')
@endsection