<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Models\Status;
use Laravel\Jetstream\Jetstream;
use Illuminate\Support\Facades\Hash;
use App\Models\RegistrationCodeModel;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['required', 'accepted'] : '',
        ])->validate();

        $user = User::create([
            'matricule' => $input['matricule'],
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        if(!empty($user) && $input['type'] == "APP"){
            $reg = RegistrationCodeModel::where('code', $input['registration_code'])->first();
            $reg->update([
                'status_id' => Status::where('slug', 'used')->first()->id
            ]);

            $user->assignRole($reg->role);
            $user->assignRole('Member');

        }

       

     


        return $user;

       
    }
}
