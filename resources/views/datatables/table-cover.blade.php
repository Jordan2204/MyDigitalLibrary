<div class="flex border-b rounded sm:justify-center">
    <div class="intro-x w-10 h-10 mx-auto cursor-pointer zoom-in image-fit">
        <img alt="a" class="rounded-full" src="/storage/{{ $cover }}">
    </div>
</div>