<?php

namespace App\Http\Livewire;

use App\Models\Testimonial;
use Livewire\Component;

class Testimonials extends Component
{
    public $testimonial_id;
    public $profile_photo_path;
    public $title;
    public $username;
    public $description;
    public $showModalForm = false;
    public $showDeleteModalForm = false;

    protected $listeners = [
        'showEditModal' => 'showEditModal',
        'showDeleteModal' => 'showDeleteModal'
    ];


    public function showCreateModal()
    {
        $this->resetValidation();
        $this->showModalForm = true;
    }
    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }

    public function store()
    {
        $this->validate([
            'title' =>'required',
            'username' =>'required',
            'description' =>'required'
        ]);

        $field = Testimonial::create([
            'title' => $this->title,
            'username' => $this->username,
            'profile_photo_path' =>"/dist/images/user_profile.png",
            'description' => $this->description,
            'created_at' => Now(),
            'updated_at' => Now(),
        ]);
        $this->reset();
        $this->emit('newQuery');
        //session()->flash('flash.banner', 'Permission created Successfully');
    }
    public function update()
    {
        $this->validate([
            'title' =>'required',
            'username' =>'required',
            'description' =>'required'
        ]);

        Testimonial::find($this->testimonial_id)->update([
            'title' => $this->title,
            'username' => $this->username,
            'profile_photo_path' =>"/dist/images/user_profile.png",
            'description' => $this->description,
            'updated_at' => Now(),
        ]);
        $this->reset();
        $this->emit('updateQuery');
        //session()->flash('flash.banner', 'Post Updated Successfully');
    }

    public function showEditModal($id)
    {
        $this->reset();
        $this->showModalForm = true;
        $this->testimonial_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $testimonial = Testimonial::findOrFail($this->testimonial_id);
        $this->title = $testimonial->title;
        $this->profile_photo_path = $testimonial->profile_photo_path;
        $this->username = $testimonial->username;
        $this->description = $testimonial->description;
    }

    public function showDeleteModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->testimonial_id = $id;
    }

    public function delete($id)
    {
        $testimonial = Testimonial::find($id);
        $testimonial->delete();
        $this->showDeleteModalForm = false;
        $this->emit('deleteQuery');
    }

    public function render()
    {
        return view('livewire.testimonials');
    }
}
