<?php

namespace App\Http\Livewire;

use App\Models\Author;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class Authors extends Component
{
    public $name;
    public $description;
    public $author_id;
    public $showModalForm = false;
    public $showDeleteModalForm = false;

    public function showCreateAuthorModal()
    {
        $this->resetValidation();
        $this->showModalForm = true;
    }
    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function storeAuthor()
    {
        $this->validate([
          'name' =>'required'
      ]);
        $role = Author::create(['name' => $this->name]);
        $this->alert('success', 'Author '.$this->name.' deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
      ]);
      $this->reset();
     
    }
    public function updateAuthor()
    {
        $this->validate([
          'name' =>'required',
      ]);

        Author::find($this->author_id)->update([
             'name' => $this->name,
             'description' => $this->description
        ]);
        $this->alert('success', 'Author '.$this->name.' updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
      ]);
      $this->reset();
   
    }

    public function showEditAuthorModal($id)
    {
        $this->reset();
        $this->showModalForm = true;
        $this->author_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $author = Author::findOrFail($this->author_id);
        $this->name = $author->name;
        $this->description = $author->description;
    }

    public function showDeleteAuthorModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->author_id = $id;
    }

    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }

    public function deleteAuthor($id)
    {
        $author = Author::find($id);
        $author->delete();
        $this->showDeleteModalForm = false;
        $this->alert('success', 'Author '.$author->name.' deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
      ]);
        $this->reset();
   
    }

    public function render()
    {
        
        return view('livewire.authors', ['authors' => Author::all()]);
    }
}
