@extends('../layouts/' . $layout)

@section('subhead')
    <title>{{ __("Library Home Configurations")}}</title>
@endsection

@section('subcontent')
    @livewire('home-settings')
@endsection