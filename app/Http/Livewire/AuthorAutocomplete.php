<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Author;

class AuthorAutocomplete extends Autocomplete
{
    protected $listeners = ['valueSelected', 'resetInputs', 'updateAuthor1Search', 'updateAuthor2Search'];

    public function valueSelected(Author $author)
    {
        $this->emitUp('authorSelected', $author);
    }

    public function resetInputs(){
        $this->search = "";
    }

    public function updateAuthor1Search(Author $author){
        $this->search = $author->name;
    }

    public function updateAuthor2Search(Author $author){
        $this->search = $author->name;
    }

    public function query() {
        return Author::where('name', 'like', '%'.$this->search.'%')->orderBy('name');
    }   
}
