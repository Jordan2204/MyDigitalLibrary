<?php

namespace App\Http\Livewire\DataTable;

use App\Models\Testimonial;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class TestimonialsTable extends LivewireDatatable
{
    public $model = Testimonial::class;

    public $hideable = 'select';
    public $exportable = true;
    
    protected $listeners =
     [
        'newQuery' => 'refresh_data',
        'updateQuery' => 'refresh_data',
        'deleteQuery' => 'refresh_data'
    ];

    public function builder()
    {
        return  Testimonial::query();
    }

    public function showEditModal($id){
        $this->emitUp('showEditModal', $id);
    }

    public function showDeleteModal($id){
        $this->emitUp('showDeleteModal', $id);
    }

    
    public function refresh_data()
    {
        $this->emit('updateSavedQueries');
    }

  
    public function columns()
    {
        return [

            Column::callback(['id', 'profile_photo_path'], function ($id, $profile_photo_path) {
                return view('datatables.table-cover', 
                ['id' => $id, 'cover' => $profile_photo_path]);
            })->label(__('Profile'))
            ->unsortable(),

            Column::name('title')
                ->label(__('Title'))
                ->searchable()
                ->alignCenter(),
            
            Column::name('username')
            ->label(__('User'))
            ->searchable()
            ->alignCenter(),

            Column::name('description')
            ->label(__('Description'))
            ->truncate(50)
            ->searchable(),

            Column::callback(['id', 'title'], function ($id, $title) {
                return view('datatables.table-actions', 
                ['id' => $id, 'name' => $title, 'type' => 'testimonial']);
            })->label('Actions')
            ->unsortable()       

        ];
    }

}