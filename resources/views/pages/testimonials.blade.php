@extends('../layouts/' . $layout)

@section('subhead')
    <title>{{ __('Testimonial Management') }}</title>
@endsection

@section('subcontent')
    @livewire('testimonials')
@endsection