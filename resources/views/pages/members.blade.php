@extends('../layouts/' . $layout)

@section('subhead')
    <title>Users Management</title>
@endsection

@section('subcontent')
   @livewire('members')
@endsection