<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use App\Actions\Fortify\PasswordValidationRules;
use Laravel\Fortify\Contracts\ResetsUserPasswords;

class Admins extends Component
{
   
    use WithPagination;
    use PasswordValidationRules;
    use WithFileUploads;
 

    public $admin_id;
    public $matricule;
    public $name;
    public $email;
    public $phone;
    public $country;
    public $city;
    public $date_of_birth;
    public $role;
    public $image;  
    public $newImage;
    public $password;
    public $saved_password;
    public $password_confirmation;

    public $showModalForm = false;
    public $showDeleteModalForm = false;

    public function showCreateAdminModal()
    {
        $this->resetValidation();
        $this->showModalForm = true;
    }
    public function updatedShowModalForm()
    {
        $this->reset();
    }
    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }

    public function storeAdmin()
    {
        $this->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'image' => 'image|max:4072|nullable',
            'password' => $this->passwordRules(),
        ]);

        $url_image ="";
        if ($this->image) {
                $url_extension = $this->image->getClientOriginalExtension();
                $image_name = 'profile-photos'.'/'.Str::random(40);
                $image_name = $image_name.'.'.$url_extension;
                $this->image->storeAs('public/', $image_name);
            }
        $admin = User::create([
            'matricule' => $this->matricule,
            'name' => $this->name,
            'date_of_birth' => $this->date_of_birth,
            'profile_photo_path' => $image_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'password' => Hash::make($this->password),
        ]);
        $admin->assignRole($this->role);

        $this->reset();
       
        $this->alert('success', 'Admin Created Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }
    public function updateAdmin()
    {
        //dd($this->date_of_birth);
        $this->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'image' => 'image|max:4072|nullable',
      ]);

      $new_url = $this->newImage;
      $password = $this->saved_password;
      if($this->password){
        $password = Hash::make($this->password);
      }
      
      if ($this->image && !is_string($this->image)) {
        Storage::delete('public/', $this->newImage);
        $new_url = 'profile-photos/'.Str::random(40).'.'.$this->image->getClientOriginalExtension();
        $this->image->storeAs('public/', $new_url);
        }


        User::find($this->admin_id)->update([
            'matricule' => $this->matricule,
            'name' => $this->name,
            'date_of_birth' => $this->date_of_birth,
            'email' => $this->email,
            'password' => $password,
            'profile_photo_path' => $new_url,
            'phone' => $this->phone
      
        ]);
        $this->reset();
        $this->alert('success', 'Admin updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

    public function showEditAdminModal($id)
    {
        $this->reset();
        $this->showModalForm = true;
        $this->admin_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $admin = User::findOrFail($this->admin_id);
        $this->matricule =  $admin->matricule;
        $this->name =  $admin->name;
        $this->date_of_birth =  $admin->date_of_birth;
        $this->email =  $admin->email;
        $this->saved_password =  $admin->password;
        $this->phone =  $admin->phone;
        $this->role = $admin->getRoleNames();
        $this->newImage = $admin->profile_photo_path;
        $this->image = $admin->profile_photo_path;
    }

    public function showDeleteAdminModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->admin_id = $id;
    }

    public function deleteAdmin($id)
    {
        $admin = User::find($id);
        if( $admin->profile_photo_path){
            Storage::delete('public/', $admin->profile_photo_path);
        }
        $admin->delete();
        $this->showDeleteModalForm = false;
        $this->alert('success', 'Admin deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);  }

    public function render()
    {
        return view('livewire.admins',
        [
        'admins' => User::role(['Admin','Librarian'])->with('roles')->get(),
        'roles' => Role::where("name", "Admin")->orWhere("name", "Librarian")->get()
        ]);
    }
}
