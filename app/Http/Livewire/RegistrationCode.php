<?php

namespace App\Http\Livewire;

use App\Models\Status;
use Livewire\Component;
use Illuminate\Support\Str;
use App\Models\RegistrationCodeModel;
use Spatie\Permission\Models\Role;

class RegistrationCode extends Component
{
    public $regist_id;
    public $code;
    public $status_id;
    public $number;
    public $role;

    public $showModalForm = false;
    public $showDeleteModalForm = false;

    protected $listeners = [
        'showEditModal' => 'showEditModal',
        'showDeleteModal' => 'showDeleteModal'
    ];

    public function showCreateModal()
    {
        $this->resetValidation();
        $this->reset();

        $this->showModalForm = true;
    }

    public function closeModal()
    {
        $this->reset();
        $this->showDeleteModalForm = false;
    }

    public function updatedShowModalForm()
    {
        $this->reset();
    }

    public function store()
    {
        $this->validate([
          'number' =>'required',
          'status_id' =>'required'
      ]);

      for ($i=0; $i < $this->number; $i++) { 
        $code = Str::random(10);
        RegistrationCodeModel::create(
            [
            'code' => $code,
            'status_id' => $this->status_id,
            'role' => $this->role
            ]);
      }
     
        $this->reset();
        $this->emit('newQuery');

        $this->alert('success', 'Registration Code created Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }
    public function update()
    {
        $this->validate([
          'status_id' =>'required',
      ]);

      RegistrationCodeModel::find($this->regist_id)->update([
            'status_id' => $this->status_id
        ]);
        $this->reset();
        $this->emit('updateQuery');
        
        $this->alert('success', 'Registration Code updated Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
    }

    public function showEditModal($id)
    {
        $this->reset();
        $this->showModalForm = true;
        $this->regist_id = $id;
        $this->loadEditForm();
    }

    public function loadEditForm()
    {
        $registration_code = RegistrationCodeModel::findOrFail($this->regist_id);
        $this->code = $registration_code->code;
        $this->role = $registration_code->role;
        $this->status_id = $registration_code->status_id;
    }

    public function showDeleteModal($id)
    {
        $this->reset();
        $this->showDeleteModalForm = true;
        $this->regist_id = $id;
    }

    public function delete($id)
    {
        $registration_code = RegistrationCodeModel::find($id);
        $registration_code->delete();
        $this->showDeleteModalForm = false;
        $this->emit('deleteQuery');

        $this->alert('success', 'Registration Code deleted Successfully', [
            'position' =>  'top-end', 
            'timer' =>  '6000', 
            'toast' =>  true, 
            'text' =>  '', 
            'confirmButtonText' =>  'Ok', 
            'cancelButtonText' =>  'Cancel', 
            'showCancelButton' =>  false, 
            'showConfirmButton' =>  false, 
        ]);
     }


    public function render()
    {
        $status = Status::all();
           
        return view('livewire.registration-code',
    [
        'status' => $status,
        'roles' => Role::where("name", "Student")->orWhere("name", "Lecturer")->orWhere("name", "Staff")->get()
    
    ]);
    }
}
