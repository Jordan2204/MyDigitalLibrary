<div>
    <div class="relative z-0 intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">{{ __('Registration Code Management') }}</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <x-jet-button wire:click="showCreateModal" class=" bg-theme-1 shadow-md mr-2 text-center">{{ __('New') }}</x-jet-button>    
        </div>
    </div>
    <!-- BEGIN: Datatable -->
    <div class="intro-y datatable-wrapper box p-2 pt-2 mt-5">
        @livewire('data-table.registration-code-table')
    </div>

    <!-- Modal create Registration Code-->
    <x-jet-dialog-modal :maxWidth="'xl'" class="z-40" wire:model="showModalForm">
        @if($regist_id)
        <x-slot name="ico">
            <svg xmlns="http://www.w3.org/2000/svg" class="mt-2 ml-2 h-6 w-6 text-yellow-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
            </svg>
        </x-slot>
        <x-slot name="title">{{ __('Updating a Registration Code') }}</x-slot>   
        @else
            <x-slot name="ico">
                <svg xmlns="http://www.w3.org/2000/svg" class="mt-2 ml-2 h-6 w-6 text-green-600" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </x-slot>
            <x-slot name="title">{{ __('Creation of a new Registration Code') }}</x-slot>  
        @endif
        <x-slot name="close">
            <a x-on:click.prevent @click="@this.closeModal()" href=""> 
                <svg xmlns="http://www.w3.org/2000/svg" class="mt-1 ml-2 text-white h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </a> 
        </x-slot>
    <x-slot name="content">
    <div class="space-y-4 divide-y divide-gray-200">
            <form enctype="multipart/form-data"> 
                @csrf
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 text sm:pb-4">
                    <div class="grid grid-cols-1 sm:grid-cols-3 col-span-2 gap-4">
                        @if(!$this->code)
                            <div class="col-span-3 sm:col-span-1"> 
                                <x-jet-label for="book_item_code" value="{{ __('Number to Generate') }}" />
                                <input id="number" mame="number" wire:model="number" type="number" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2" required autofocus autocomplete="code">
                                @error('number') <span class="text-red-500">{{ $message }}</span>@enderror 
                            </div> 
                        @else
                            <div class="col-span-3 sm:col-span-1"> 
                                <x-jet-label for="book_item_code" value="{{ __('Code') }}" />
                                <input id="code" disabled mame="code" wire:model="code" type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2" required autofocus autocomplete="code">
                                @error('code') <span class="text-red-500">{{ $message }}</span>@enderror 
                            </div> 
                        @endif
                          
                        <div class="col-span-3 sm:col-span-1"> 
                            <x-jet-label for="status_id" value="{{ __('Status') }}" />
                            <select id="status_id" wire:model="status_id" name="status_id" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                @if($this->status_id == null)
                                    <option selected value="Null">{{ __('choose a status') }}</option>
                                @endif
                                @foreach ($status as $statut)
                                    <option value="{{ $statut->id }}">{{ $statut->title }}</option>
                                @endforeach
                            </select>      
                                @error('status_id') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div>

                        <div class="col-span-2 sm:col-span-1"> 
                            <x-jet-label for="status_id" value="{{ __('Role') }}" />
                            <select id="role" wire:model="role" name="role" data-hide-search="true" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mt-2">
                                @if($this->role == null)
                                    <option selected value="Null">{{ __('choose a role') }}</option>
                                @endif
                                @foreach ($roles as $role)
                                    <option value="{{ $role->name }}">{{ $role->name }}</option>
                                @endforeach
                            </select>      
                            @error('role') <span class="text-red-500">{{ $message }}</span>@enderror 
                        </div>
                      
                    </div> 
                </div> 
        
            </form> 
        </div>
    </x-slot>

    <x-slot name="footer">
    @if($regist_id)
    <x-jet-button wire:click="update">  {{ __('Update') }}</x-jet-button>
    @else
    <x-jet-button wire:click="store"> {{ __('Create') }}</x-jet-button>
    @endif
    </x-slot>
    </x-jet-dialog-modal>


    <x-jet-confirmation-modal wire:model="showDeleteModalForm">
            <x-slot name="title">{{ __('Deletion of a Registration Code') }}</x-slot>
            <x-slot name="close">
                <a x-on:click.prevent @click="@this.closeModal()" href=""> 
                    <svg xmlns="http://www.w3.org/2000/svg" class="mt-1 ml-2 text-white h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </a> 
            </x-slot>
            <x-slot name="content">
                <div class="space-y-8 text-2xl divide-y divide-gray-200 ">
                    <div class="p-5 text-center"> <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                        @if($this->regist_id)
                            <p>{{ __('Do you really want to delete this ') }}<span class="text-gray-900 text-bold">{{ __('Data')}} ?</span>{{ __('This process cannot be undone')}}.</p>
                        @endif
                    </div>      
                </div>
            </x-slot>

            <x-slot name="footer">
                <x-jet-button wire:click="deleteAdmin({{ $this->regist_id }})" class="bg-red-700"> 
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
                    </svg>
                    {{ __('Delete') }}
                    </x-jet-button>
            </x-slot>
    </x-jet-confirmation-modal>
</div>
