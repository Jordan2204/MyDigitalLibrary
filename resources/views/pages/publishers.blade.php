@extends('../layouts/' . $layout)

@section('subhead')
    <title>Publishers Management</title>
@endsection

@section('subcontent')
   @livewire('publishers')
@endsection