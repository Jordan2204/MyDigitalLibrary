require('./bootstrap');

require('alpinejs');

// Reference from published scripts
require('./vendor/livewire-ui/modal');

// Reference from vendor
require('../../vendor/livewire-ui/modal/resources/js/modal');

//Import modules...
import { createApp, h } from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';


import highlight from './highlight'
import slick from './slick'
import tooltipster from './tooltipster'
import datepicker from './datepicker'
import select2 from './select2'
import dropzone from './dropzone'
import summernote from './summernote'
import validation from './validation'
import imageZoom from './image-zoom'
import svgLoader from './svg-loader'
import toast from './toast'

// Components
import maps from './maps'
import chat from './chat'
import dropdown from './dropdown'
import modal from './modal'
import showModal from './show-modal'
import tab from './tab'
import accordion from './accordion'
import search from './search'
import copyCode from './copy-code'
import showCode from './show-code'
import sideMenu from './side-menu'
import mobileMenu from './mobile-menu'
import sideMenuTooltip from './side-menu-tooltip'

import AOS from 'aos'
import Notifications from 'notiwind'
import 'aos/dist/aos.css' // You can also use <link> for styles

AOS.init({
  // Global settings:
  disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
  initClassName: 'aos-init', // class applied after initialization
  animatedClassName: 'aos-animate', // class applied on animation
  useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
  disableMutationObserver: false, // disables automatic mutations' detections (advanced)
  debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
  throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
  
  // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
  offset: 120, // offset (in px) from the original trigger point
  delay: 0, // values from 0 to 3000, with step 50ms
  duration: 1500, // values from 0 to 3000, with step 50ms
  easing: 'ease', // default easing for AOS animations
  once: false, // whether animation should happen only once - while scrolling down
  mirror: false, // whether elements should animate out while scrolling past them
  anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

});

const el = document.getElementById('app');

createApp({
    render: () =>
        h(InertiaApp, {
            initialPage: JSON.parse(el.dataset.page),
            resolveComponent: (name) => require(`./Pages/${name}`).default,
        }),
})
    .mixin({ methods: { route } })
    .mixin(require('./base'))
    .use(InertiaPlugin)
    .use(Notifications)
    .mount(el);

    InertiaProgress.init({ color: '#E74C3C' });
