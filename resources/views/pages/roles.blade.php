@extends('../layouts/' . $layout)

@section('subhead')
    <title>Roles Management</title>
@endsection

@section('subcontent')  
    @livewire('roles')
@endsection